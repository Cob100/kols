1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="kol.ignite.user"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="19"
8-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="29" />
9-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.INTERNET" />
11-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:5:5-67
11-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:5:22-64
12    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
12-->[com.pierfrancescosoffritti.androidyoutubeplayer:core:10.0.5] /Users/precious/.gradle/caches/transforms-2/files-2.1/2852a2ccbcd6e945f009f481d4d947a9/core-10.0.5/AndroidManifest.xml:12:5-79
12-->[com.pierfrancescosoffritti.androidyoutubeplayer:core:10.0.5] /Users/precious/.gradle/caches/transforms-2/files-2.1/2852a2ccbcd6e945f009f481d4d947a9/core-10.0.5/AndroidManifest.xml:12:22-76
13
14    <application
14-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:7:5-80:19
15        android:name="kol.ignite.user.Kols"
15-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:8:9-29
16        android:allowBackup="true"
16-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:9:9-35
17        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
17-->[androidx.core:core:1.1.0] /Users/precious/.gradle/caches/transforms-2/files-2.1/c527b78844be871c44103f6f45a963fb/core-1.1.0/AndroidManifest.xml:24:18-86
18        android:debuggable="true"
19        android:icon="@mipmap/ic_launcher"
19-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:11:9-43
20        android:label="@string/app_name"
20-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:12:9-41
21        android:roundIcon="@mipmap/ic_launcher_round"
21-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:13:9-54
22        android:supportsRtl="true"
22-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:14:9-35
23        android:theme="@style/AppTheme"
23-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:15:9-40
24        android:usesCleartextTraffic="true" >
24-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:10:9-44
25        <activity
25-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:16:9-19:25
26            android:name="kol.ignite.user.ui.reset.ResetActivity"
26-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:16:19-57
27            android:screenOrientation="portrait"
27-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:18:13-49
28            android:theme="@style/AppTheme.NoActionBar" />
28-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:17:13-56
29        <activity
29-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:20:9-30:20
30            android:name="kol.ignite.user.ui.splash.SplashActivity"
30-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:21:13-53
31            android:screenOrientation="portrait"
31-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:23:13-49
32            android:theme="@style/AppTheme.NoActionBar" >
32-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:22:13-56
33            <intent-filter>
33-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:25:13-29:29
34                <action android:name="android.intent.action.MAIN" />
34-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:26:17-69
34-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:26:25-66
35
36                <category android:name="android.intent.category.LAUNCHER" />
36-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:28:17-77
36-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:28:27-74
37            </intent-filter>
38        </activity>
39        <activity
39-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:31:9-35:15
40            android:name="kol.ignite.user.ui.detailinbox.InboxDetailActivity"
40-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:32:13-63
41            android:screenOrientation="portrait"
41-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:34:13-49
42            android:theme="@style/AppTheme.NoActionBar" />
42-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:33:13-56
43        <activity
43-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:36:9-40:15
44            android:name="kol.ignite.user.ui.profile.ProfileActivity"
44-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:37:13-55
45            android:screenOrientation="portrait"
45-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:39:13-49
46            android:theme="@style/AppTheme.NoActionBar" />
46-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:38:13-56
47        <activity
47-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:41:9-46:15
48            android:name="kol.ignite.user.ui.detail.DetailedActivity"
48-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:42:13-55
49            android:screenOrientation="portrait"
49-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:45:13-49
50            android:theme="@style/AppTheme.NoActionBar"
50-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:43:13-56
51            android:windowSoftInputMode="stateAlwaysHidden" />
51-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:44:13-60
52        <activity
52-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:47:9-52:15
53            android:name="kol.ignite.user.ui.stream.StreamActivity"
53-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:48:13-53
54            android:screenOrientation="portrait"
54-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:51:13-49
55            android:theme="@style/AppTheme.NoActionBar"
55-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:49:13-56
56            android:windowSoftInputMode="stateAlwaysHidden" />
56-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:50:13-60
57        <activity
57-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:53:9-57:15
58            android:name="kol.ignite.user.ui.loading.LoadingActivity"
58-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:54:13-55
59            android:screenOrientation="portrait"
59-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:56:13-49
60            android:theme="@style/AppTheme.NoActionBar" />
60-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:55:13-56
61        <activity
61-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:58:9-62:15
62            android:name="kol.ignite.user.ui.recover.RecoverActivity"
62-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:59:13-55
63            android:screenOrientation="portrait"
63-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:61:13-49
64            android:theme="@style/AppTheme.NoActionBar" />
64-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:60:13-56
65        <activity
65-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:63:9-67:15
66            android:name="kol.ignite.user.ui.login.LoginActivity"
66-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:64:13-51
67            android:screenOrientation="portrait"
67-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:66:13-49
68            android:theme="@style/AppTheme.NoActionBar" />
68-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:65:13-56
69        <activity
69-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:68:9-72:15
70            android:name="kol.ignite.user.ui.signup.SignUpActivity"
70-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:69:13-53
71            android:screenOrientation="portrait"
71-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:71:13-49
72            android:theme="@style/AppTheme.NoActionBar" />
72-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:70:13-56
73        <activity
73-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:73:9-78:15
74            android:name="kol.ignite.user.ui.home.MainActivity"
74-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:74:13-49
75            android:screenOrientation="portrait"
75-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:77:13-49
76            android:theme="@style/AppTheme.NoActionBar"
76-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:75:13-56
77            android:windowSoftInputMode="stateAlwaysHidden" />
77-->/Users/precious/Documents/kol/koluser/app/src/main/AndroidManifest.xml:76:13-60
78
79        <provider
79-->[androidx.lifecycle:lifecycle-process:2.1.0] /Users/precious/.gradle/caches/transforms-2/files-2.1/c75b512c68f7bb7a7c5a931aae563be5/lifecycle-process-2.1.0/AndroidManifest.xml:23:9-27:43
80            android:name="androidx.lifecycle.ProcessLifecycleOwnerInitializer"
80-->[androidx.lifecycle:lifecycle-process:2.1.0] /Users/precious/.gradle/caches/transforms-2/files-2.1/c75b512c68f7bb7a7c5a931aae563be5/lifecycle-process-2.1.0/AndroidManifest.xml:24:13-79
81            android:authorities="kol.ignite.user.lifecycle-process"
81-->[androidx.lifecycle:lifecycle-process:2.1.0] /Users/precious/.gradle/caches/transforms-2/files-2.1/c75b512c68f7bb7a7c5a931aae563be5/lifecycle-process-2.1.0/AndroidManifest.xml:25:13-69
82            android:exported="false"
82-->[androidx.lifecycle:lifecycle-process:2.1.0] /Users/precious/.gradle/caches/transforms-2/files-2.1/c75b512c68f7bb7a7c5a931aae563be5/lifecycle-process-2.1.0/AndroidManifest.xml:26:13-37
83            android:multiprocess="true" />
83-->[androidx.lifecycle:lifecycle-process:2.1.0] /Users/precious/.gradle/caches/transforms-2/files-2.1/c75b512c68f7bb7a7c5a931aae563be5/lifecycle-process-2.1.0/AndroidManifest.xml:27:13-40
84    </application>
85
86</manifest>
