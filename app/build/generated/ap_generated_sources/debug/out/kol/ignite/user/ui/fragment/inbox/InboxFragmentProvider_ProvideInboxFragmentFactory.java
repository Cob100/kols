package kol.ignite.user.ui.fragment.inbox;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = InboxFragmentProvider_ProvideInboxFragmentFactory.InboxFragmentSubcomponent.class
)
public abstract class InboxFragmentProvider_ProvideInboxFragmentFactory {
  private InboxFragmentProvider_ProvideInboxFragmentFactory() {}

  @Binds
  @IntoMap
  @ClassKey(InboxFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      InboxFragmentSubcomponent.Builder builder);

  @Subcomponent(modules = InboxFragmentModule.class)
  public interface InboxFragmentSubcomponent extends AndroidInjector<InboxFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<InboxFragment> {}
  }
}
