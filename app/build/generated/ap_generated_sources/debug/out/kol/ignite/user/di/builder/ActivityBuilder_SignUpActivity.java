package kol.ignite.user.di.builder;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import kol.ignite.user.ui.signup.SignUpActivity;
import kol.ignite.user.ui.signup.SignUpActivityModule;

@Module(subcomponents = ActivityBuilder_SignUpActivity.SignUpActivitySubcomponent.class)
public abstract class ActivityBuilder_SignUpActivity {
  private ActivityBuilder_SignUpActivity() {}

  @Binds
  @IntoMap
  @ClassKey(SignUpActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SignUpActivitySubcomponent.Builder builder);

  @Subcomponent(modules = SignUpActivityModule.class)
  public interface SignUpActivitySubcomponent extends AndroidInjector<SignUpActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<SignUpActivity> {}
  }
}
