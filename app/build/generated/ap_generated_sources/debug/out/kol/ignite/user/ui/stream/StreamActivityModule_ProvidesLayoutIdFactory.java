// Generated by Dagger (https://google.github.io/dagger).
package kol.ignite.user.ui.stream;

import dagger.internal.Factory;

public final class StreamActivityModule_ProvidesLayoutIdFactory implements Factory<Integer> {
  private final StreamActivityModule module;

  public StreamActivityModule_ProvidesLayoutIdFactory(StreamActivityModule module) {
    this.module = module;
  }

  @Override
  public Integer get() {
    return proxyProvidesLayoutId(module);
  }

  public static StreamActivityModule_ProvidesLayoutIdFactory create(StreamActivityModule module) {
    return new StreamActivityModule_ProvidesLayoutIdFactory(module);
  }

  public static int proxyProvidesLayoutId(StreamActivityModule instance) {
    return instance.providesLayoutId();
  }
}
