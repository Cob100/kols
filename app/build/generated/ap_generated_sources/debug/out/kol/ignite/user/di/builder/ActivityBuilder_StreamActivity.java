package kol.ignite.user.di.builder;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import kol.ignite.user.ui.stream.StreamActivity;
import kol.ignite.user.ui.stream.StreamActivityModule;

@Module(subcomponents = ActivityBuilder_StreamActivity.StreamActivitySubcomponent.class)
public abstract class ActivityBuilder_StreamActivity {
  private ActivityBuilder_StreamActivity() {}

  @Binds
  @IntoMap
  @ClassKey(StreamActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      StreamActivitySubcomponent.Builder builder);

  @Subcomponent(modules = StreamActivityModule.class)
  public interface StreamActivitySubcomponent extends AndroidInjector<StreamActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<StreamActivity> {}
  }
}
