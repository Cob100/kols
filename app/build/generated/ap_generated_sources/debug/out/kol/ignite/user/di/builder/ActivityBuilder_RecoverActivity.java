package kol.ignite.user.di.builder;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import kol.ignite.user.ui.recover.RecoverActivity;
import kol.ignite.user.ui.recover.RecoverActivityModule;

@Module(subcomponents = ActivityBuilder_RecoverActivity.RecoverActivitySubcomponent.class)
public abstract class ActivityBuilder_RecoverActivity {
  private ActivityBuilder_RecoverActivity() {}

  @Binds
  @IntoMap
  @ClassKey(RecoverActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      RecoverActivitySubcomponent.Builder builder);

  @Subcomponent(modules = RecoverActivityModule.class)
  public interface RecoverActivitySubcomponent extends AndroidInjector<RecoverActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<RecoverActivity> {}
  }
}
