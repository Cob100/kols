package kol.ignite.user.databinding;
import kol.ignite.user.R;
import kol.ignite.user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityLoginBindingImpl extends ActivityLoginBinding implements kol.ignite.user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.forgot_your, 6);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @NonNull
    private final android.widget.TextView mboundView5;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback16;
    @Nullable
    private final android.view.View.OnClickListener mCallback17;
    @Nullable
    private final android.view.View.OnClickListener mCallback15;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener emailandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of loginviewmodel.email.get()
            //         is loginviewmodel.email.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(email);
            // localize variables for thread safety
            // loginviewmodel.email.get()
            java.lang.String loginviewmodelEmailGet = null;
            // loginviewmodel
            kol.ignite.user.ui.login.LoginViewModel loginviewmodel = mLoginviewmodel;
            // loginviewmodel != null
            boolean loginviewmodelJavaLangObjectNull = false;
            // loginviewmodel.email
            androidx.databinding.ObservableField<java.lang.String> loginviewmodelEmail = null;
            // loginviewmodel.email != null
            boolean loginviewmodelEmailJavaLangObjectNull = false;



            loginviewmodelJavaLangObjectNull = (loginviewmodel) != (null);
            if (loginviewmodelJavaLangObjectNull) {


                loginviewmodelEmail = loginviewmodel.email;

                loginviewmodelEmailJavaLangObjectNull = (loginviewmodelEmail) != (null);
                if (loginviewmodelEmailJavaLangObjectNull) {




                    loginviewmodelEmail.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener passwordandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of loginviewmodel.password.get()
            //         is loginviewmodel.password.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(password);
            // localize variables for thread safety
            // loginviewmodel.password.get()
            java.lang.String loginviewmodelPasswordGet = null;
            // loginviewmodel.password
            androidx.databinding.ObservableField<java.lang.String> loginviewmodelPassword = null;
            // loginviewmodel
            kol.ignite.user.ui.login.LoginViewModel loginviewmodel = mLoginviewmodel;
            // loginviewmodel.password != null
            boolean loginviewmodelPasswordJavaLangObjectNull = false;
            // loginviewmodel != null
            boolean loginviewmodelJavaLangObjectNull = false;



            loginviewmodelJavaLangObjectNull = (loginviewmodel) != (null);
            if (loginviewmodelJavaLangObjectNull) {


                loginviewmodelPassword = loginviewmodel.password;

                loginviewmodelPasswordJavaLangObjectNull = (loginviewmodelPassword) != (null);
                if (loginviewmodelPasswordJavaLangObjectNull) {




                    loginviewmodelPassword.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public ActivityLoginBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ActivityLoginBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.EditText) bindings[2]
            , (android.widget.TextView) bindings[6]
            , (android.widget.EditText) bindings[3]
            , (android.widget.TextView) bindings[4]
            );
        this.email.setTag(null);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView5 = (android.widget.TextView) bindings[5];
        this.mboundView5.setTag(null);
        this.password.setTag(null);
        this.recover.setTag(null);
        setRootTag(root);
        // listeners
        mCallback16 = new kol.ignite.user.generated.callback.OnClickListener(this, 2);
        mCallback17 = new kol.ignite.user.generated.callback.OnClickListener(this, 3);
        mCallback15 = new kol.ignite.user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.loginviewmodel == variableId) {
            setLoginviewmodel((kol.ignite.user.ui.login.LoginViewModel) variable);
        }
        else if (BR.navigator == variableId) {
            setNavigator((kol.ignite.user.ui.login.LoginNavigator) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setLoginviewmodel(@Nullable kol.ignite.user.ui.login.LoginViewModel Loginviewmodel) {
        this.mLoginviewmodel = Loginviewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.loginviewmodel);
        super.requestRebind();
    }
    public void setNavigator(@Nullable kol.ignite.user.ui.login.LoginNavigator Navigator) {
        this.mNavigator = Navigator;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.navigator);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeLoginviewmodelPassword((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeLoginviewmodelEmail((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeLoginviewmodelPassword(androidx.databinding.ObservableField<java.lang.String> LoginviewmodelPassword, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeLoginviewmodelEmail(androidx.databinding.ObservableField<java.lang.String> LoginviewmodelEmail, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String loginviewmodelEmailGet = null;
        java.lang.String loginviewmodelPasswordGet = null;
        androidx.databinding.ObservableField<java.lang.String> loginviewmodelPassword = null;
        kol.ignite.user.ui.login.LoginViewModel loginviewmodel = mLoginviewmodel;
        androidx.databinding.ObservableField<java.lang.String> loginviewmodelEmail = null;
        kol.ignite.user.ui.login.LoginNavigator navigator = mNavigator;

        if ((dirtyFlags & 0x17L) != 0) {


            if ((dirtyFlags & 0x15L) != 0) {

                    if (loginviewmodel != null) {
                        // read loginviewmodel.password
                        loginviewmodelPassword = loginviewmodel.password;
                    }
                    updateRegistration(0, loginviewmodelPassword);


                    if (loginviewmodelPassword != null) {
                        // read loginviewmodel.password.get()
                        loginviewmodelPasswordGet = loginviewmodelPassword.get();
                    }
            }
            if ((dirtyFlags & 0x16L) != 0) {

                    if (loginviewmodel != null) {
                        // read loginviewmodel.email
                        loginviewmodelEmail = loginviewmodel.email;
                    }
                    updateRegistration(1, loginviewmodelEmail);


                    if (loginviewmodelEmail != null) {
                        // read loginviewmodel.email.get()
                        loginviewmodelEmailGet = loginviewmodelEmail.get();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x16L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.email, loginviewmodelEmailGet);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.email, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, emailandroidTextAttrChanged);
            this.mboundView1.setOnClickListener(mCallback15);
            this.mboundView5.setOnClickListener(mCallback17);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.password, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, passwordandroidTextAttrChanged);
            this.recover.setOnClickListener(mCallback16);
        }
        if ((dirtyFlags & 0x15L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.password, loginviewmodelPasswordGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.login.LoginNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.loadRecover();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.login.LoginNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.loadLogin();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.login.LoginNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.onBack();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): loginviewmodel.password
        flag 1 (0x2L): loginviewmodel.email
        flag 2 (0x3L): loginviewmodel
        flag 3 (0x4L): navigator
        flag 4 (0x5L): null
    flag mapping end*/
    //end
}