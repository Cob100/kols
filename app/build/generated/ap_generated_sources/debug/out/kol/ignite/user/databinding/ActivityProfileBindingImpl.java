package kol.ignite.user.databinding;
import kol.ignite.user.R;
import kol.ignite.user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityProfileBindingImpl extends ActivityProfileBinding implements kol.ignite.user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.tv_fullname, 3);
        sViewsWithIds.put(R.id.profile_image, 4);
        sViewsWithIds.put(R.id.tv_username, 5);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @NonNull
    private final android.widget.TextView mboundView2;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback21;
    @Nullable
    private final android.view.View.OnClickListener mCallback20;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private ActivityProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[4]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[5]
            );
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        setRootTag(root);
        // listeners
        mCallback21 = new kol.ignite.user.generated.callback.OnClickListener(this, 2);
        mCallback20 = new kol.ignite.user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.profileviewmodel == variableId) {
            setProfileviewmodel((kol.ignite.user.ui.profile.ProfileViewModel) variable);
        }
        else if (BR.navigator == variableId) {
            setNavigator((kol.ignite.user.ui.profile.ProfileNavigator) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setProfileviewmodel(@Nullable kol.ignite.user.ui.profile.ProfileViewModel Profileviewmodel) {
        this.mProfileviewmodel = Profileviewmodel;
    }
    public void setNavigator(@Nullable kol.ignite.user.ui.profile.ProfileNavigator Navigator) {
        this.mNavigator = Navigator;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.navigator);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        kol.ignite.user.ui.profile.ProfileNavigator navigator = mNavigator;
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.mboundView1.setOnClickListener(mCallback20);
            this.mboundView2.setOnClickListener(mCallback21);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.profile.ProfileNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.vote();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.profile.ProfileNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.onBack();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): profileviewmodel
        flag 1 (0x2L): navigator
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}