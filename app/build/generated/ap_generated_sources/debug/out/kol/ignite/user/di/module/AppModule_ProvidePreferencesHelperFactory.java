// Generated by Dagger (https://google.github.io/dagger).
package kol.ignite.user.di.module;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;
import kol.ignite.user.data.local.prefs.AppPreferencesHelper;
import kol.ignite.user.data.local.prefs.PreferencesHelper;

public final class AppModule_ProvidePreferencesHelperFactory implements Factory<PreferencesHelper> {
  private final AppModule module;

  private final Provider<AppPreferencesHelper> appPreferencesHelperProvider;

  public AppModule_ProvidePreferencesHelperFactory(
      AppModule module, Provider<AppPreferencesHelper> appPreferencesHelperProvider) {
    this.module = module;
    this.appPreferencesHelperProvider = appPreferencesHelperProvider;
  }

  @Override
  public PreferencesHelper get() {
    return proxyProvidePreferencesHelper(module, appPreferencesHelperProvider.get());
  }

  public static AppModule_ProvidePreferencesHelperFactory create(
      AppModule module, Provider<AppPreferencesHelper> appPreferencesHelperProvider) {
    return new AppModule_ProvidePreferencesHelperFactory(module, appPreferencesHelperProvider);
  }

  public static PreferencesHelper proxyProvidePreferencesHelper(
      AppModule instance, AppPreferencesHelper appPreferencesHelper) {
    return Preconditions.checkNotNull(
        instance.providePreferencesHelper(appPreferencesHelper),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
