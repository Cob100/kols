package kol.ignite.user.databinding;
import kol.ignite.user.R;
import kol.ignite.user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityRecoverBindingImpl extends ActivityRecoverBinding implements kol.ignite.user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.please_prov, 4);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @NonNull
    private final android.widget.TextView mboundView3;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback7;
    @Nullable
    private final android.view.View.OnClickListener mCallback6;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener emailandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of recoverviewmodel.email.get()
            //         is recoverviewmodel.email.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(email);
            // localize variables for thread safety
            // recoverviewmodel.email
            androidx.databinding.ObservableField<java.lang.String> recoverviewmodelEmail = null;
            // recoverviewmodel.email != null
            boolean recoverviewmodelEmailJavaLangObjectNull = false;
            // recoverviewmodel
            kol.ignite.user.ui.recover.RecoverViewModel recoverviewmodel = mRecoverviewmodel;
            // recoverviewmodel.email.get()
            java.lang.String recoverviewmodelEmailGet = null;
            // recoverviewmodel != null
            boolean recoverviewmodelJavaLangObjectNull = false;



            recoverviewmodelJavaLangObjectNull = (recoverviewmodel) != (null);
            if (recoverviewmodelJavaLangObjectNull) {


                recoverviewmodelEmail = recoverviewmodel.email;

                recoverviewmodelEmailJavaLangObjectNull = (recoverviewmodelEmail) != (null);
                if (recoverviewmodelEmailJavaLangObjectNull) {




                    recoverviewmodelEmail.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public ActivityRecoverBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ActivityRecoverBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.EditText) bindings[2]
            , (android.widget.TextView) bindings[4]
            );
        this.email.setTag(null);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        setRootTag(root);
        // listeners
        mCallback7 = new kol.ignite.user.generated.callback.OnClickListener(this, 2);
        mCallback6 = new kol.ignite.user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.recoverviewmodel == variableId) {
            setRecoverviewmodel((kol.ignite.user.ui.recover.RecoverViewModel) variable);
        }
        else if (BR.navigator == variableId) {
            setNavigator((kol.ignite.user.ui.recover.RecoverNavigator) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setRecoverviewmodel(@Nullable kol.ignite.user.ui.recover.RecoverViewModel Recoverviewmodel) {
        this.mRecoverviewmodel = Recoverviewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.recoverviewmodel);
        super.requestRebind();
    }
    public void setNavigator(@Nullable kol.ignite.user.ui.recover.RecoverNavigator Navigator) {
        this.mNavigator = Navigator;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.navigator);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeRecoverviewmodelEmail((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeRecoverviewmodelEmail(androidx.databinding.ObservableField<java.lang.String> RecoverviewmodelEmail, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableField<java.lang.String> recoverviewmodelEmail = null;
        kol.ignite.user.ui.recover.RecoverViewModel recoverviewmodel = mRecoverviewmodel;
        java.lang.String recoverviewmodelEmailGet = null;
        kol.ignite.user.ui.recover.RecoverNavigator navigator = mNavigator;

        if ((dirtyFlags & 0xbL) != 0) {



                if (recoverviewmodel != null) {
                    // read recoverviewmodel.email
                    recoverviewmodelEmail = recoverviewmodel.email;
                }
                updateRegistration(0, recoverviewmodelEmail);


                if (recoverviewmodelEmail != null) {
                    // read recoverviewmodel.email.get()
                    recoverviewmodelEmailGet = recoverviewmodelEmail.get();
                }
        }
        // batch finished
        if ((dirtyFlags & 0xbL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.email, recoverviewmodelEmailGet);
        }
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.email, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, emailandroidTextAttrChanged);
            this.mboundView1.setOnClickListener(mCallback6);
            this.mboundView3.setOnClickListener(mCallback7);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.recover.RecoverNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.loadSubmit();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.recover.RecoverNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.onBack();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): recoverviewmodel.email
        flag 1 (0x2L): recoverviewmodel
        flag 2 (0x3L): navigator
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}