package kol.ignite.user.databinding;
import kol.ignite.user.R;
import kol.ignite.user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityMainBindingImpl extends ActivityMainBinding implements kol.ignite.user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.container, 6);
        sViewsWithIds.put(R.id.bottom_bar, 7);
        sViewsWithIds.put(R.id.home_image, 8);
        sViewsWithIds.put(R.id.home_text, 9);
        sViewsWithIds.put(R.id.episode_image, 10);
        sViewsWithIds.put(R.id.episode_text, 11);
        sViewsWithIds.put(R.id.vote_image, 12);
        sViewsWithIds.put(R.id.vote_text, 13);
        sViewsWithIds.put(R.id.inbox_image, 14);
        sViewsWithIds.put(R.id.inbox_text, 15);
        sViewsWithIds.put(R.id.account_image, 16);
        sViewsWithIds.put(R.id.account_text, 17);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @NonNull
    private final android.widget.LinearLayout mboundView2;
    @NonNull
    private final android.widget.LinearLayout mboundView3;
    @NonNull
    private final android.widget.LinearLayout mboundView4;
    @NonNull
    private final android.widget.LinearLayout mboundView5;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback5;
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    @Nullable
    private final android.view.View.OnClickListener mCallback4;
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityMainBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 18, sIncludes, sViewsWithIds));
    }
    private ActivityMainBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[16]
            , (android.widget.TextView) bindings[17]
            , (android.widget.LinearLayout) bindings[7]
            , (android.widget.FrameLayout) bindings[6]
            , (android.widget.ImageView) bindings[10]
            , (android.widget.TextView) bindings[11]
            , (android.widget.ImageView) bindings[8]
            , (android.widget.TextView) bindings[9]
            , (android.widget.ImageView) bindings[14]
            , (android.widget.TextView) bindings[15]
            , (android.widget.ImageView) bindings[12]
            , (android.widget.TextView) bindings[13]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.LinearLayout) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.LinearLayout) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.LinearLayout) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.widget.LinearLayout) bindings[5];
        this.mboundView5.setTag(null);
        setRootTag(root);
        // listeners
        mCallback5 = new kol.ignite.user.generated.callback.OnClickListener(this, 5);
        mCallback3 = new kol.ignite.user.generated.callback.OnClickListener(this, 3);
        mCallback4 = new kol.ignite.user.generated.callback.OnClickListener(this, 4);
        mCallback1 = new kol.ignite.user.generated.callback.OnClickListener(this, 1);
        mCallback2 = new kol.ignite.user.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.mainViewModel == variableId) {
            setMainViewModel((kol.ignite.user.ui.home.MainViewModel) variable);
        }
        else if (BR.navigator == variableId) {
            setNavigator((kol.ignite.user.ui.home.MainNavigator) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setMainViewModel(@Nullable kol.ignite.user.ui.home.MainViewModel MainViewModel) {
        this.mMainViewModel = MainViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.mainViewModel);
        super.requestRebind();
    }
    public void setNavigator(@Nullable kol.ignite.user.ui.home.MainNavigator Navigator) {
        this.mNavigator = Navigator;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        kol.ignite.user.ui.home.MainViewModel mainViewModel = mMainViewModel;
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.mboundView1.setOnClickListener(mCallback1);
            this.mboundView2.setOnClickListener(mCallback2);
            this.mboundView3.setOnClickListener(mCallback3);
            this.mboundView4.setOnClickListener(mCallback4);
            this.mboundView5.setOnClickListener(mCallback5);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 5: {
                // localize variables for thread safety
                // mainViewModel
                kol.ignite.user.ui.home.MainViewModel mainViewModel = mMainViewModel;
                // mainViewModel != null
                boolean mainViewModelJavaLangObjectNull = false;



                mainViewModelJavaLangObjectNull = (mainViewModel) != (null);
                if (mainViewModelJavaLangObjectNull) {



                    mainViewModel.onTabSelected(5);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // mainViewModel
                kol.ignite.user.ui.home.MainViewModel mainViewModel = mMainViewModel;
                // mainViewModel != null
                boolean mainViewModelJavaLangObjectNull = false;



                mainViewModelJavaLangObjectNull = (mainViewModel) != (null);
                if (mainViewModelJavaLangObjectNull) {



                    mainViewModel.onTabSelected(3);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // mainViewModel
                kol.ignite.user.ui.home.MainViewModel mainViewModel = mMainViewModel;
                // mainViewModel != null
                boolean mainViewModelJavaLangObjectNull = false;



                mainViewModelJavaLangObjectNull = (mainViewModel) != (null);
                if (mainViewModelJavaLangObjectNull) {



                    mainViewModel.onTabSelected(4);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // mainViewModel
                kol.ignite.user.ui.home.MainViewModel mainViewModel = mMainViewModel;
                // mainViewModel != null
                boolean mainViewModelJavaLangObjectNull = false;



                mainViewModelJavaLangObjectNull = (mainViewModel) != (null);
                if (mainViewModelJavaLangObjectNull) {



                    mainViewModel.onTabSelected(1);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // mainViewModel
                kol.ignite.user.ui.home.MainViewModel mainViewModel = mMainViewModel;
                // mainViewModel != null
                boolean mainViewModelJavaLangObjectNull = false;



                mainViewModelJavaLangObjectNull = (mainViewModel) != (null);
                if (mainViewModelJavaLangObjectNull) {



                    mainViewModel.onTabSelected(2);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): mainViewModel
        flag 1 (0x2L): navigator
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}