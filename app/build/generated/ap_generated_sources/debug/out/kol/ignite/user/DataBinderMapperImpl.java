package kol.ignite.user;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kol.ignite.user.databinding.ActivityDetailedBindingImpl;
import kol.ignite.user.databinding.ActivityInboxDetailBindingImpl;
import kol.ignite.user.databinding.ActivityLoadingBindingImpl;
import kol.ignite.user.databinding.ActivityLoginBindingImpl;
import kol.ignite.user.databinding.ActivityMainBindingImpl;
import kol.ignite.user.databinding.ActivityProfileBindingImpl;
import kol.ignite.user.databinding.ActivityRecoverBindingImpl;
import kol.ignite.user.databinding.ActivityResetBindingImpl;
import kol.ignite.user.databinding.ActivitySignUpBindingImpl;
import kol.ignite.user.databinding.ActivityStreamBindingImpl;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYDETAILED = 1;

  private static final int LAYOUT_ACTIVITYINBOXDETAIL = 2;

  private static final int LAYOUT_ACTIVITYLOADING = 3;

  private static final int LAYOUT_ACTIVITYLOGIN = 4;

  private static final int LAYOUT_ACTIVITYMAIN = 5;

  private static final int LAYOUT_ACTIVITYPROFILE = 6;

  private static final int LAYOUT_ACTIVITYRECOVER = 7;

  private static final int LAYOUT_ACTIVITYRESET = 8;

  private static final int LAYOUT_ACTIVITYSIGNUP = 9;

  private static final int LAYOUT_ACTIVITYSTREAM = 10;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(10);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(kol.ignite.user.R.layout.activity_detailed, LAYOUT_ACTIVITYDETAILED);
    INTERNAL_LAYOUT_ID_LOOKUP.put(kol.ignite.user.R.layout.activity_inbox_detail, LAYOUT_ACTIVITYINBOXDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(kol.ignite.user.R.layout.activity_loading, LAYOUT_ACTIVITYLOADING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(kol.ignite.user.R.layout.activity_login, LAYOUT_ACTIVITYLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(kol.ignite.user.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(kol.ignite.user.R.layout.activity_profile, LAYOUT_ACTIVITYPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(kol.ignite.user.R.layout.activity_recover, LAYOUT_ACTIVITYRECOVER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(kol.ignite.user.R.layout.activity_reset, LAYOUT_ACTIVITYRESET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(kol.ignite.user.R.layout.activity_sign_up, LAYOUT_ACTIVITYSIGNUP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(kol.ignite.user.R.layout.activity_stream, LAYOUT_ACTIVITYSTREAM);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYDETAILED: {
          if ("layout/activity_detailed_0".equals(tag)) {
            return new ActivityDetailedBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_detailed is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYINBOXDETAIL: {
          if ("layout/activity_inbox_detail_0".equals(tag)) {
            return new ActivityInboxDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_inbox_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYLOADING: {
          if ("layout/activity_loading_0".equals(tag)) {
            return new ActivityLoadingBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_loading is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYLOGIN: {
          if ("layout/activity_login_0".equals(tag)) {
            return new ActivityLoginBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_login is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYPROFILE: {
          if ("layout/activity_profile_0".equals(tag)) {
            return new ActivityProfileBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_profile is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYRECOVER: {
          if ("layout/activity_recover_0".equals(tag)) {
            return new ActivityRecoverBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_recover is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYRESET: {
          if ("layout/activity_reset_0".equals(tag)) {
            return new ActivityResetBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_reset is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSIGNUP: {
          if ("layout/activity_sign_up_0".equals(tag)) {
            return new ActivitySignUpBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_sign_up is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSTREAM: {
          if ("layout/activity_stream_0".equals(tag)) {
            return new ActivityStreamBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_stream is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(13);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "resetviewmodel");
      sKeys.put(2, "inboxdetailviewmodel");
      sKeys.put(3, "navigator");
      sKeys.put(4, "streamviewmodel");
      sKeys.put(5, "loadingviewmodel");
      sKeys.put(6, "recoverviewmodel");
      sKeys.put(7, "profileviewmodel");
      sKeys.put(8, "signviewmodel");
      sKeys.put(9, "detailviewmodel");
      sKeys.put(10, "loginviewmodel");
      sKeys.put(11, "mainViewModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(10);

    static {
      sKeys.put("layout/activity_detailed_0", kol.ignite.user.R.layout.activity_detailed);
      sKeys.put("layout/activity_inbox_detail_0", kol.ignite.user.R.layout.activity_inbox_detail);
      sKeys.put("layout/activity_loading_0", kol.ignite.user.R.layout.activity_loading);
      sKeys.put("layout/activity_login_0", kol.ignite.user.R.layout.activity_login);
      sKeys.put("layout/activity_main_0", kol.ignite.user.R.layout.activity_main);
      sKeys.put("layout/activity_profile_0", kol.ignite.user.R.layout.activity_profile);
      sKeys.put("layout/activity_recover_0", kol.ignite.user.R.layout.activity_recover);
      sKeys.put("layout/activity_reset_0", kol.ignite.user.R.layout.activity_reset);
      sKeys.put("layout/activity_sign_up_0", kol.ignite.user.R.layout.activity_sign_up);
      sKeys.put("layout/activity_stream_0", kol.ignite.user.R.layout.activity_stream);
    }
  }
}
