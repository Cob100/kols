package kol.ignite.user.databinding;
import kol.ignite.user.R;
import kol.ignite.user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityResetBindingImpl extends ActivityResetBinding implements kol.ignite.user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.please_prov, 5);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @NonNull
    private final android.widget.TextView mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback9;
    @Nullable
    private final android.view.View.OnClickListener mCallback8;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener newpandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of resetviewmodel.newp.get()
            //         is resetviewmodel.newp.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(newp);
            // localize variables for thread safety
            // resetviewmodel.newp != null
            boolean resetviewmodelNewpJavaLangObjectNull = false;
            // resetviewmodel.newp
            androidx.databinding.ObservableField<java.lang.String> resetviewmodelNewp = null;
            // resetviewmodel.newp.get()
            java.lang.String resetviewmodelNewpGet = null;
            // resetviewmodel
            kol.ignite.user.ui.reset.ResetViewModel resetviewmodel = mResetviewmodel;
            // resetviewmodel != null
            boolean resetviewmodelJavaLangObjectNull = false;



            resetviewmodelJavaLangObjectNull = (resetviewmodel) != (null);
            if (resetviewmodelJavaLangObjectNull) {


                resetviewmodelNewp = resetviewmodel.newp;

                resetviewmodelNewpJavaLangObjectNull = (resetviewmodelNewp) != (null);
                if (resetviewmodelNewpJavaLangObjectNull) {




                    resetviewmodelNewp.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener retypeandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of resetviewmodel.retype.get()
            //         is resetviewmodel.retype.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(retype);
            // localize variables for thread safety
            // resetviewmodel.retype != null
            boolean resetviewmodelRetypeJavaLangObjectNull = false;
            // resetviewmodel.retype
            androidx.databinding.ObservableField<java.lang.String> resetviewmodelRetype = null;
            // resetviewmodel.retype.get()
            java.lang.String resetviewmodelRetypeGet = null;
            // resetviewmodel
            kol.ignite.user.ui.reset.ResetViewModel resetviewmodel = mResetviewmodel;
            // resetviewmodel != null
            boolean resetviewmodelJavaLangObjectNull = false;



            resetviewmodelJavaLangObjectNull = (resetviewmodel) != (null);
            if (resetviewmodelJavaLangObjectNull) {


                resetviewmodelRetype = resetviewmodel.retype;

                resetviewmodelRetypeJavaLangObjectNull = (resetviewmodelRetype) != (null);
                if (resetviewmodelRetypeJavaLangObjectNull) {




                    resetviewmodelRetype.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public ActivityResetBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private ActivityResetBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.EditText) bindings[2]
            , (android.widget.TextView) bindings[5]
            , (android.widget.EditText) bindings[3]
            );
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.newp.setTag(null);
        this.retype.setTag(null);
        setRootTag(root);
        // listeners
        mCallback9 = new kol.ignite.user.generated.callback.OnClickListener(this, 2);
        mCallback8 = new kol.ignite.user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.resetviewmodel == variableId) {
            setResetviewmodel((kol.ignite.user.ui.reset.ResetViewModel) variable);
        }
        else if (BR.navigator == variableId) {
            setNavigator((kol.ignite.user.ui.reset.ResetNavigator) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setResetviewmodel(@Nullable kol.ignite.user.ui.reset.ResetViewModel Resetviewmodel) {
        this.mResetviewmodel = Resetviewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.resetviewmodel);
        super.requestRebind();
    }
    public void setNavigator(@Nullable kol.ignite.user.ui.reset.ResetNavigator Navigator) {
        this.mNavigator = Navigator;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.navigator);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeResetviewmodelRetype((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeResetviewmodelNewp((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeResetviewmodelRetype(androidx.databinding.ObservableField<java.lang.String> ResetviewmodelRetype, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeResetviewmodelNewp(androidx.databinding.ObservableField<java.lang.String> ResetviewmodelNewp, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableField<java.lang.String> resetviewmodelRetype = null;
        androidx.databinding.ObservableField<java.lang.String> resetviewmodelNewp = null;
        java.lang.String resetviewmodelRetypeGet = null;
        java.lang.String resetviewmodelNewpGet = null;
        kol.ignite.user.ui.reset.ResetViewModel resetviewmodel = mResetviewmodel;
        kol.ignite.user.ui.reset.ResetNavigator navigator = mNavigator;

        if ((dirtyFlags & 0x17L) != 0) {


            if ((dirtyFlags & 0x15L) != 0) {

                    if (resetviewmodel != null) {
                        // read resetviewmodel.retype
                        resetviewmodelRetype = resetviewmodel.retype;
                    }
                    updateRegistration(0, resetviewmodelRetype);


                    if (resetviewmodelRetype != null) {
                        // read resetviewmodel.retype.get()
                        resetviewmodelRetypeGet = resetviewmodelRetype.get();
                    }
            }
            if ((dirtyFlags & 0x16L) != 0) {

                    if (resetviewmodel != null) {
                        // read resetviewmodel.newp
                        resetviewmodelNewp = resetviewmodel.newp;
                    }
                    updateRegistration(1, resetviewmodelNewp);


                    if (resetviewmodelNewp != null) {
                        // read resetviewmodel.newp.get()
                        resetviewmodelNewpGet = resetviewmodelNewp.get();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.mboundView1.setOnClickListener(mCallback8);
            this.mboundView4.setOnClickListener(mCallback9);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.newp, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, newpandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.retype, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, retypeandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x16L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.newp, resetviewmodelNewpGet);
        }
        if ((dirtyFlags & 0x15L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.retype, resetviewmodelRetypeGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.reset.ResetNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.loadSubmit();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.reset.ResetNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.onBack();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): resetviewmodel.retype
        flag 1 (0x2L): resetviewmodel.newp
        flag 2 (0x3L): resetviewmodel
        flag 3 (0x4L): navigator
        flag 4 (0x5L): null
    flag mapping end*/
    //end
}