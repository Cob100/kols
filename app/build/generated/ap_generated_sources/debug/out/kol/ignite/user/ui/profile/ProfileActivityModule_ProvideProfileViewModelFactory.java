// Generated by Dagger (https://google.github.io/dagger).
package kol.ignite.user.ui.profile;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

public final class ProfileActivityModule_ProvideProfileViewModelFactory
    implements Factory<ProfileViewModel> {
  private final ProfileActivityModule module;

  private final Provider<DataManager> dataManagerProvider;

  private final Provider<SchedulerProvider> schedulerProvider;

  public ProfileActivityModule_ProvideProfileViewModelFactory(
      ProfileActivityModule module,
      Provider<DataManager> dataManagerProvider,
      Provider<SchedulerProvider> schedulerProvider) {
    this.module = module;
    this.dataManagerProvider = dataManagerProvider;
    this.schedulerProvider = schedulerProvider;
  }

  @Override
  public ProfileViewModel get() {
    return proxyProvideProfileViewModel(module, dataManagerProvider.get(), schedulerProvider.get());
  }

  public static ProfileActivityModule_ProvideProfileViewModelFactory create(
      ProfileActivityModule module,
      Provider<DataManager> dataManagerProvider,
      Provider<SchedulerProvider> schedulerProvider) {
    return new ProfileActivityModule_ProvideProfileViewModelFactory(
        module, dataManagerProvider, schedulerProvider);
  }

  public static ProfileViewModel proxyProvideProfileViewModel(
      ProfileActivityModule instance,
      DataManager dataManager,
      SchedulerProvider schedulerProvider) {
    return Preconditions.checkNotNull(
        instance.provideProfileViewModel(dataManager, schedulerProvider),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
