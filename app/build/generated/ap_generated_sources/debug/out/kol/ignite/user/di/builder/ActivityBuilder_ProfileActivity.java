package kol.ignite.user.di.builder;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import kol.ignite.user.ui.profile.ProfileActivity;
import kol.ignite.user.ui.profile.ProfileActivityModule;

@Module(subcomponents = ActivityBuilder_ProfileActivity.ProfileActivitySubcomponent.class)
public abstract class ActivityBuilder_ProfileActivity {
  private ActivityBuilder_ProfileActivity() {}

  @Binds
  @IntoMap
  @ClassKey(ProfileActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      ProfileActivitySubcomponent.Builder builder);

  @Subcomponent(modules = ProfileActivityModule.class)
  public interface ProfileActivitySubcomponent extends AndroidInjector<ProfileActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<ProfileActivity> {}
  }
}
