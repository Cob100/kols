package kol.ignite.user.di.builder;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import kol.ignite.user.ui.detailinbox.InboxDetailActivity;
import kol.ignite.user.ui.detailinbox.InboxDetailActivityModule;

@Module(subcomponents = ActivityBuilder_InboxDetailActivity.InboxDetailActivitySubcomponent.class)
public abstract class ActivityBuilder_InboxDetailActivity {
  private ActivityBuilder_InboxDetailActivity() {}

  @Binds
  @IntoMap
  @ClassKey(InboxDetailActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      InboxDetailActivitySubcomponent.Builder builder);

  @Subcomponent(modules = InboxDetailActivityModule.class)
  public interface InboxDetailActivitySubcomponent extends AndroidInjector<InboxDetailActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<InboxDetailActivity> {}
  }
}
