// Generated by Dagger (https://google.github.io/dagger).
package kol.ignite.user.di.module;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;
import kol.ignite.user.data.remote.ApiHelper;
import kol.ignite.user.data.remote.AppApiHelper;

public final class AppModule_ProvideApiHelperFactory implements Factory<ApiHelper> {
  private final AppModule module;

  private final Provider<AppApiHelper> appApiHelperProvider;

  public AppModule_ProvideApiHelperFactory(
      AppModule module, Provider<AppApiHelper> appApiHelperProvider) {
    this.module = module;
    this.appApiHelperProvider = appApiHelperProvider;
  }

  @Override
  public ApiHelper get() {
    return proxyProvideApiHelper(module, appApiHelperProvider.get());
  }

  public static AppModule_ProvideApiHelperFactory create(
      AppModule module, Provider<AppApiHelper> appApiHelperProvider) {
    return new AppModule_ProvideApiHelperFactory(module, appApiHelperProvider);
  }

  public static ApiHelper proxyProvideApiHelper(AppModule instance, AppApiHelper appApiHelper) {
    return Preconditions.checkNotNull(
        instance.provideApiHelper(appApiHelper),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
