package kol.ignite.user.di.builder;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import kol.ignite.user.ui.reset.ResetActivity;
import kol.ignite.user.ui.reset.ResetActivityModule;

@Module(subcomponents = ActivityBuilder_ResetActivity.ResetActivitySubcomponent.class)
public abstract class ActivityBuilder_ResetActivity {
  private ActivityBuilder_ResetActivity() {}

  @Binds
  @IntoMap
  @ClassKey(ResetActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      ResetActivitySubcomponent.Builder builder);

  @Subcomponent(modules = ResetActivityModule.class)
  public interface ResetActivitySubcomponent extends AndroidInjector<ResetActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<ResetActivity> {}
  }
}
