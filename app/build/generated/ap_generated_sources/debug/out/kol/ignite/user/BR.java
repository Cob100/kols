package kol.ignite.user;

public class BR {
  public static final int _all = 0;

  public static final int resetviewmodel = 1;

  public static final int inboxdetailviewmodel = 2;

  public static final int navigator = 3;

  public static final int streamviewmodel = 4;

  public static final int loadingviewmodel = 5;

  public static final int recoverviewmodel = 6;

  public static final int profileviewmodel = 7;

  public static final int signviewmodel = 8;

  public static final int detailviewmodel = 9;

  public static final int loginviewmodel = 10;

  public static final int mainViewModel = 11;
}
