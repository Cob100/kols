package kol.ignite.user.di.builder;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import kol.ignite.user.ui.detail.DetailedActivity;
import kol.ignite.user.ui.detail.DetailedActivityModule;

@Module(subcomponents = ActivityBuilder_DetailedActivity.DetailedActivitySubcomponent.class)
public abstract class ActivityBuilder_DetailedActivity {
  private ActivityBuilder_DetailedActivity() {}

  @Binds
  @IntoMap
  @ClassKey(DetailedActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      DetailedActivitySubcomponent.Builder builder);

  @Subcomponent(modules = DetailedActivityModule.class)
  public interface DetailedActivitySubcomponent extends AndroidInjector<DetailedActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<DetailedActivity> {}
  }
}
