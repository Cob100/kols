package kol.ignite.user.di.builder;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import kol.ignite.user.ui.loading.LoadingActivity;
import kol.ignite.user.ui.loading.LoadingActivityModule;

@Module(subcomponents = ActivityBuilder_LoadingActivity.LoadingActivitySubcomponent.class)
public abstract class ActivityBuilder_LoadingActivity {
  private ActivityBuilder_LoadingActivity() {}

  @Binds
  @IntoMap
  @ClassKey(LoadingActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      LoadingActivitySubcomponent.Builder builder);

  @Subcomponent(modules = LoadingActivityModule.class)
  public interface LoadingActivitySubcomponent extends AndroidInjector<LoadingActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<LoadingActivity> {}
  }
}
