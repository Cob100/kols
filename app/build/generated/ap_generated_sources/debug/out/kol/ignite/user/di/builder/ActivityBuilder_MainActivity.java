package kol.ignite.user.di.builder;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import kol.ignite.user.ui.home.MainActivity;
import kol.ignite.user.ui.home.MainActivityModule;

@Module(subcomponents = ActivityBuilder_MainActivity.MainActivitySubcomponent.class)
public abstract class ActivityBuilder_MainActivity {
  private ActivityBuilder_MainActivity() {}

  @Binds
  @IntoMap
  @ClassKey(MainActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      MainActivitySubcomponent.Builder builder);

  @Subcomponent(modules = MainActivityModule.class)
  public interface MainActivitySubcomponent extends AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity> {}
  }
}
