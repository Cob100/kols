package kol.ignite.user.databinding;
import kol.ignite.user.R;
import kol.ignite.user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityDetailedBindingImpl extends ActivityDetailedBinding implements kol.ignite.user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.title, 4);
        sViewsWithIds.put(R.id.li, 5);
        sViewsWithIds.put(R.id.youtube_player_view, 6);
        sViewsWithIds.put(R.id.desp, 7);
        sViewsWithIds.put(R.id.rv_comment, 8);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @NonNull
    private final android.widget.ImageView mboundView3;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback14;
    @Nullable
    private final android.view.View.OnClickListener mCallback13;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener edCommentandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of detailviewmodel.comment.get()
            //         is detailviewmodel.comment.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(edComment);
            // localize variables for thread safety
            // detailviewmodel
            kol.ignite.user.ui.detail.DetailedViewModel detailviewmodel = mDetailviewmodel;
            // detailviewmodel.comment.get()
            java.lang.String detailviewmodelCommentGet = null;
            // detailviewmodel.comment
            androidx.databinding.ObservableField<java.lang.String> detailviewmodelComment = null;
            // detailviewmodel != null
            boolean detailviewmodelJavaLangObjectNull = false;
            // detailviewmodel.comment != null
            boolean detailviewmodelCommentJavaLangObjectNull = false;



            detailviewmodelJavaLangObjectNull = (detailviewmodel) != (null);
            if (detailviewmodelJavaLangObjectNull) {


                detailviewmodelComment = detailviewmodel.comment;

                detailviewmodelCommentJavaLangObjectNull = (detailviewmodelComment) != (null);
                if (detailviewmodelCommentJavaLangObjectNull) {




                    detailviewmodelComment.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public ActivityDetailedBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 9, sIncludes, sViewsWithIds));
    }
    private ActivityDetailedBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.TextView) bindings[7]
            , (android.widget.EditText) bindings[2]
            , (android.widget.LinearLayout) bindings[5]
            , (androidx.recyclerview.widget.RecyclerView) bindings[8]
            , (android.widget.TextView) bindings[4]
            , (com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView) bindings[6]
            );
        this.edComment.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView3 = (android.widget.ImageView) bindings[3];
        this.mboundView3.setTag(null);
        setRootTag(root);
        // listeners
        mCallback14 = new kol.ignite.user.generated.callback.OnClickListener(this, 2);
        mCallback13 = new kol.ignite.user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.detailviewmodel == variableId) {
            setDetailviewmodel((kol.ignite.user.ui.detail.DetailedViewModel) variable);
        }
        else if (BR.navigator == variableId) {
            setNavigator((kol.ignite.user.ui.detail.DetailedNavigator) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setDetailviewmodel(@Nullable kol.ignite.user.ui.detail.DetailedViewModel Detailviewmodel) {
        this.mDetailviewmodel = Detailviewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.detailviewmodel);
        super.requestRebind();
    }
    public void setNavigator(@Nullable kol.ignite.user.ui.detail.DetailedNavigator Navigator) {
        this.mNavigator = Navigator;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.navigator);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeDetailviewmodelComment((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeDetailviewmodelComment(androidx.databinding.ObservableField<java.lang.String> DetailviewmodelComment, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        kol.ignite.user.ui.detail.DetailedViewModel detailviewmodel = mDetailviewmodel;
        java.lang.String detailviewmodelCommentGet = null;
        androidx.databinding.ObservableField<java.lang.String> detailviewmodelComment = null;
        kol.ignite.user.ui.detail.DetailedNavigator navigator = mNavigator;

        if ((dirtyFlags & 0xbL) != 0) {



                if (detailviewmodel != null) {
                    // read detailviewmodel.comment
                    detailviewmodelComment = detailviewmodel.comment;
                }
                updateRegistration(0, detailviewmodelComment);


                if (detailviewmodelComment != null) {
                    // read detailviewmodel.comment.get()
                    detailviewmodelCommentGet = detailviewmodelComment.get();
                }
        }
        // batch finished
        if ((dirtyFlags & 0xbL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.edComment, detailviewmodelCommentGet);
        }
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.edComment, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, edCommentandroidTextAttrChanged);
            this.mboundView1.setOnClickListener(mCallback13);
            this.mboundView3.setOnClickListener(mCallback14);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.detail.DetailedNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.submitComment();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.detail.DetailedNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.onBack();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): detailviewmodel.comment
        flag 1 (0x2L): detailviewmodel
        flag 2 (0x3L): navigator
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}