package kol.ignite.user.databinding;
import kol.ignite.user.R;
import kol.ignite.user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivitySignUpBindingImpl extends ActivitySignUpBinding implements kol.ignite.user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.please_prov, 9);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @NonNull
    private final android.widget.TextView mboundView8;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback11;
    @Nullable
    private final android.view.View.OnClickListener mCallback12;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener emailandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of signviewmodel.email.get()
            //         is signviewmodel.email.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(email);
            // localize variables for thread safety
            // signviewmodel.email.get()
            java.lang.String signviewmodelEmailGet = null;
            // signviewmodel != null
            boolean signviewmodelJavaLangObjectNull = false;
            // signviewmodel.email
            androidx.databinding.ObservableField<java.lang.String> signviewmodelEmail = null;
            // signviewmodel
            kol.ignite.user.ui.signup.SignUpViewModel signviewmodel = mSignviewmodel;
            // signviewmodel.email != null
            boolean signviewmodelEmailJavaLangObjectNull = false;



            signviewmodelJavaLangObjectNull = (signviewmodel) != (null);
            if (signviewmodelJavaLangObjectNull) {


                signviewmodelEmail = signviewmodel.email;

                signviewmodelEmailJavaLangObjectNull = (signviewmodelEmail) != (null);
                if (signviewmodelEmailJavaLangObjectNull) {




                    signviewmodelEmail.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener fullNameandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of signviewmodel.fullName.get()
            //         is signviewmodel.fullName.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(fullName);
            // localize variables for thread safety
            // signviewmodel != null
            boolean signviewmodelJavaLangObjectNull = false;
            // signviewmodel.fullName
            androidx.databinding.ObservableField<java.lang.String> signviewmodelFullName = null;
            // signviewmodel.fullName != null
            boolean signviewmodelFullNameJavaLangObjectNull = false;
            // signviewmodel
            kol.ignite.user.ui.signup.SignUpViewModel signviewmodel = mSignviewmodel;
            // signviewmodel.fullName.get()
            java.lang.String signviewmodelFullNameGet = null;



            signviewmodelJavaLangObjectNull = (signviewmodel) != (null);
            if (signviewmodelJavaLangObjectNull) {


                signviewmodelFullName = signviewmodel.fullName;

                signviewmodelFullNameJavaLangObjectNull = (signviewmodelFullName) != (null);
                if (signviewmodelFullNameJavaLangObjectNull) {




                    signviewmodelFullName.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener passwordandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of signviewmodel.password.get()
            //         is signviewmodel.password.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(password);
            // localize variables for thread safety
            // signviewmodel.password
            androidx.databinding.ObservableField<java.lang.String> signviewmodelPassword = null;
            // signviewmodel.password.get()
            java.lang.String signviewmodelPasswordGet = null;
            // signviewmodel != null
            boolean signviewmodelJavaLangObjectNull = false;
            // signviewmodel.password != null
            boolean signviewmodelPasswordJavaLangObjectNull = false;
            // signviewmodel
            kol.ignite.user.ui.signup.SignUpViewModel signviewmodel = mSignviewmodel;



            signviewmodelJavaLangObjectNull = (signviewmodel) != (null);
            if (signviewmodelJavaLangObjectNull) {


                signviewmodelPassword = signviewmodel.password;

                signviewmodelPasswordJavaLangObjectNull = (signviewmodelPassword) != (null);
                if (signviewmodelPasswordJavaLangObjectNull) {




                    signviewmodelPassword.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener phonenumberandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of signviewmodel.phonenumber.get()
            //         is signviewmodel.phonenumber.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(phonenumber);
            // localize variables for thread safety
            // signviewmodel.phonenumber.get()
            java.lang.String signviewmodelPhonenumberGet = null;
            // signviewmodel.phonenumber
            androidx.databinding.ObservableField<java.lang.String> signviewmodelPhonenumber = null;
            // signviewmodel.phonenumber != null
            boolean signviewmodelPhonenumberJavaLangObjectNull = false;
            // signviewmodel != null
            boolean signviewmodelJavaLangObjectNull = false;
            // signviewmodel
            kol.ignite.user.ui.signup.SignUpViewModel signviewmodel = mSignviewmodel;



            signviewmodelJavaLangObjectNull = (signviewmodel) != (null);
            if (signviewmodelJavaLangObjectNull) {


                signviewmodelPhonenumber = signviewmodel.phonenumber;

                signviewmodelPhonenumberJavaLangObjectNull = (signviewmodelPhonenumber) != (null);
                if (signviewmodelPhonenumberJavaLangObjectNull) {




                    signviewmodelPhonenumber.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener retypeandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of signviewmodel.retype.get()
            //         is signviewmodel.retype.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(retype);
            // localize variables for thread safety
            // signviewmodel.retype
            androidx.databinding.ObservableField<java.lang.String> signviewmodelRetype = null;
            // signviewmodel != null
            boolean signviewmodelJavaLangObjectNull = false;
            // signviewmodel.retype.get()
            java.lang.String signviewmodelRetypeGet = null;
            // signviewmodel
            kol.ignite.user.ui.signup.SignUpViewModel signviewmodel = mSignviewmodel;
            // signviewmodel.retype != null
            boolean signviewmodelRetypeJavaLangObjectNull = false;



            signviewmodelJavaLangObjectNull = (signviewmodel) != (null);
            if (signviewmodelJavaLangObjectNull) {


                signviewmodelRetype = signviewmodel.retype;

                signviewmodelRetypeJavaLangObjectNull = (signviewmodelRetype) != (null);
                if (signviewmodelRetypeJavaLangObjectNull) {




                    signviewmodelRetype.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener usernameandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of signviewmodel.username.get()
            //         is signviewmodel.username.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(username);
            // localize variables for thread safety
            // signviewmodel != null
            boolean signviewmodelJavaLangObjectNull = false;
            // signviewmodel
            kol.ignite.user.ui.signup.SignUpViewModel signviewmodel = mSignviewmodel;
            // signviewmodel.username != null
            boolean signviewmodelUsernameJavaLangObjectNull = false;
            // signviewmodel.username.get()
            java.lang.String signviewmodelUsernameGet = null;
            // signviewmodel.username
            androidx.databinding.ObservableField<java.lang.String> signviewmodelUsername = null;



            signviewmodelJavaLangObjectNull = (signviewmodel) != (null);
            if (signviewmodelJavaLangObjectNull) {


                signviewmodelUsername = signviewmodel.username;

                signviewmodelUsernameJavaLangObjectNull = (signviewmodelUsername) != (null);
                if (signviewmodelUsernameJavaLangObjectNull) {




                    signviewmodelUsername.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public ActivitySignUpBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private ActivitySignUpBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 6
            , (android.widget.EditText) bindings[5]
            , (android.widget.EditText) bindings[2]
            , (android.widget.EditText) bindings[6]
            , (android.widget.EditText) bindings[4]
            , (android.widget.TextView) bindings[9]
            , (android.widget.EditText) bindings[7]
            , (android.widget.EditText) bindings[3]
            );
        this.email.setTag(null);
        this.fullName.setTag(null);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView8 = (android.widget.TextView) bindings[8];
        this.mboundView8.setTag(null);
        this.password.setTag(null);
        this.phonenumber.setTag(null);
        this.retype.setTag(null);
        this.username.setTag(null);
        setRootTag(root);
        // listeners
        mCallback11 = new kol.ignite.user.generated.callback.OnClickListener(this, 1);
        mCallback12 = new kol.ignite.user.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.navigator == variableId) {
            setNavigator((kol.ignite.user.ui.signup.SignUpNavigator) variable);
        }
        else if (BR.signviewmodel == variableId) {
            setSignviewmodel((kol.ignite.user.ui.signup.SignUpViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setNavigator(@Nullable kol.ignite.user.ui.signup.SignUpNavigator Navigator) {
        this.mNavigator = Navigator;
        synchronized(this) {
            mDirtyFlags |= 0x40L;
        }
        notifyPropertyChanged(BR.navigator);
        super.requestRebind();
    }
    public void setSignviewmodel(@Nullable kol.ignite.user.ui.signup.SignUpViewModel Signviewmodel) {
        this.mSignviewmodel = Signviewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.signviewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeSignviewmodelPassword((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeSignviewmodelPhonenumber((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeSignviewmodelRetype((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeSignviewmodelFullName((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeSignviewmodelEmail((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeSignviewmodelUsername((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeSignviewmodelPassword(androidx.databinding.ObservableField<java.lang.String> SignviewmodelPassword, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeSignviewmodelPhonenumber(androidx.databinding.ObservableField<java.lang.String> SignviewmodelPhonenumber, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeSignviewmodelRetype(androidx.databinding.ObservableField<java.lang.String> SignviewmodelRetype, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeSignviewmodelFullName(androidx.databinding.ObservableField<java.lang.String> SignviewmodelFullName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeSignviewmodelEmail(androidx.databinding.ObservableField<java.lang.String> SignviewmodelEmail, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeSignviewmodelUsername(androidx.databinding.ObservableField<java.lang.String> SignviewmodelUsername, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableField<java.lang.String> signviewmodelPassword = null;
        java.lang.String signviewmodelEmailGet = null;
        java.lang.String signviewmodelPasswordGet = null;
        kol.ignite.user.ui.signup.SignUpNavigator navigator = mNavigator;
        java.lang.String signviewmodelFullNameGet = null;
        androidx.databinding.ObservableField<java.lang.String> signviewmodelPhonenumber = null;
        androidx.databinding.ObservableField<java.lang.String> signviewmodelRetype = null;
        java.lang.String signviewmodelUsernameGet = null;
        java.lang.String signviewmodelPhonenumberGet = null;
        java.lang.String signviewmodelRetypeGet = null;
        androidx.databinding.ObservableField<java.lang.String> signviewmodelFullName = null;
        androidx.databinding.ObservableField<java.lang.String> signviewmodelEmail = null;
        kol.ignite.user.ui.signup.SignUpViewModel signviewmodel = mSignviewmodel;
        androidx.databinding.ObservableField<java.lang.String> signviewmodelUsername = null;

        if ((dirtyFlags & 0x1bfL) != 0) {


            if ((dirtyFlags & 0x181L) != 0) {

                    if (signviewmodel != null) {
                        // read signviewmodel.password
                        signviewmodelPassword = signviewmodel.password;
                    }
                    updateRegistration(0, signviewmodelPassword);


                    if (signviewmodelPassword != null) {
                        // read signviewmodel.password.get()
                        signviewmodelPasswordGet = signviewmodelPassword.get();
                    }
            }
            if ((dirtyFlags & 0x182L) != 0) {

                    if (signviewmodel != null) {
                        // read signviewmodel.phonenumber
                        signviewmodelPhonenumber = signviewmodel.phonenumber;
                    }
                    updateRegistration(1, signviewmodelPhonenumber);


                    if (signviewmodelPhonenumber != null) {
                        // read signviewmodel.phonenumber.get()
                        signviewmodelPhonenumberGet = signviewmodelPhonenumber.get();
                    }
            }
            if ((dirtyFlags & 0x184L) != 0) {

                    if (signviewmodel != null) {
                        // read signviewmodel.retype
                        signviewmodelRetype = signviewmodel.retype;
                    }
                    updateRegistration(2, signviewmodelRetype);


                    if (signviewmodelRetype != null) {
                        // read signviewmodel.retype.get()
                        signviewmodelRetypeGet = signviewmodelRetype.get();
                    }
            }
            if ((dirtyFlags & 0x188L) != 0) {

                    if (signviewmodel != null) {
                        // read signviewmodel.fullName
                        signviewmodelFullName = signviewmodel.fullName;
                    }
                    updateRegistration(3, signviewmodelFullName);


                    if (signviewmodelFullName != null) {
                        // read signviewmodel.fullName.get()
                        signviewmodelFullNameGet = signviewmodelFullName.get();
                    }
            }
            if ((dirtyFlags & 0x190L) != 0) {

                    if (signviewmodel != null) {
                        // read signviewmodel.email
                        signviewmodelEmail = signviewmodel.email;
                    }
                    updateRegistration(4, signviewmodelEmail);


                    if (signviewmodelEmail != null) {
                        // read signviewmodel.email.get()
                        signviewmodelEmailGet = signviewmodelEmail.get();
                    }
            }
            if ((dirtyFlags & 0x1a0L) != 0) {

                    if (signviewmodel != null) {
                        // read signviewmodel.username
                        signviewmodelUsername = signviewmodel.username;
                    }
                    updateRegistration(5, signviewmodelUsername);


                    if (signviewmodelUsername != null) {
                        // read signviewmodel.username.get()
                        signviewmodelUsernameGet = signviewmodelUsername.get();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x190L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.email, signviewmodelEmailGet);
        }
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.email, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, emailandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.fullName, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, fullNameandroidTextAttrChanged);
            this.mboundView1.setOnClickListener(mCallback11);
            this.mboundView8.setOnClickListener(mCallback12);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.password, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, passwordandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.phonenumber, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, phonenumberandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.retype, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, retypeandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.username, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, usernameandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x188L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.fullName, signviewmodelFullNameGet);
        }
        if ((dirtyFlags & 0x181L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.password, signviewmodelPasswordGet);
        }
        if ((dirtyFlags & 0x182L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.phonenumber, signviewmodelPhonenumberGet);
        }
        if ((dirtyFlags & 0x184L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.retype, signviewmodelRetypeGet);
        }
        if ((dirtyFlags & 0x1a0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.username, signviewmodelUsernameGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.signup.SignUpNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.onBack();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // navigator
                kol.ignite.user.ui.signup.SignUpNavigator navigator = mNavigator;
                // navigator != null
                boolean navigatorJavaLangObjectNull = false;



                navigatorJavaLangObjectNull = (navigator) != (null);
                if (navigatorJavaLangObjectNull) {


                    navigator.loadSubmit();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): signviewmodel.password
        flag 1 (0x2L): signviewmodel.phonenumber
        flag 2 (0x3L): signviewmodel.retype
        flag 3 (0x4L): signviewmodel.fullName
        flag 4 (0x5L): signviewmodel.email
        flag 5 (0x6L): signviewmodel.username
        flag 6 (0x7L): navigator
        flag 7 (0x8L): signviewmodel
        flag 8 (0x9L): null
    flag mapping end*/
    //end
}