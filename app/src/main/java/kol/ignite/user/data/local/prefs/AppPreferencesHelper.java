package kol.ignite.user.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import kol.ignite.user.data.model.User;
import kol.ignite.user.di.PreferenceInfo;

public class AppPreferencesHelper implements PreferencesHelper{

    private static final String PREF_KEY_LOCAL_DATABASE_UPDATED = "LOCAL_DATABASE_UPDATED_1";
    private static final String SHARED_PREF_KEY_NAME = "my_shared_pref";

    private final SharedPreferences mPrefs;
    private static AppPreferencesHelper mInstance;
    private final SharedPreferences cachedResponses;
    private Context context;


    @Inject
    AppPreferencesHelper(Context context, @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        cachedResponses = context.getSharedPreferences(prefFileName + "_response_cached", Context.MODE_PRIVATE);

    }

    public AppPreferencesHelper(Context context) {
        this.context = context;
        cachedResponses = null;
        mPrefs = null;

    }

    public static synchronized AppPreferencesHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new AppPreferencesHelper(context);
        }
        return mInstance;
    }

    @Override
    public String getAccessToken() {
        return null;
    }

    @Override
    public void setAccessToken(String token) {

    }

    @Override
    public void Logout() {

    }

    @Override
    public boolean isUserLoggedIn() {
        return false;
    }

    public void saveUser(User user) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREF_KEY_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("id", user.getId());
        editor.putString("fullname", user.getFullname());
        editor.putString("username", user.getUsername());
        editor.putString("email", user.getEmail());
        editor.putString("phonenumber", user.getPhonenumber());

        editor.apply();
    }

    public User getUser() {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREF_KEY_NAME, Context.MODE_PRIVATE);
        User user = new User(
                preferences.getString("id", null),
                preferences.getString("fullname", null),
                preferences.getString("username", null),
                preferences.getString("email", null),
                preferences.getString("phonenumber", null)
        );

        return user;
    }


    /*
    public void saveToken(Data data) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREF_KEY_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("token", data.getToken());

        editor.apply();
    }

    public Data getToken() {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREF_KEY_NAME, Context.MODE_PRIVATE);
        Data data = new Data(
                preferences.getString("token", null)
        );

        return data;
    }


    @Override
    public String getAccessToken() {
        return data1.getToken();

    }

    @Override
    public void setAccessToken(String token) {

    }

    @Override
    public boolean isUserLoggedIn() {
        String accessToken = getToken().getToken();

        if (accessToken==null){
            return false;
        }else {
            return true;
        }
        //return accessToken != null && accessToken.length() > 0;
    }


    @Override
    public void Logout() {
        setAccessToken("");
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREF_KEY_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }*/
}
