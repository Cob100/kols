package kol.ignite.user.data.remote;


import kol.ignite.user.config.BuildConfig;

public class ApiUrlConfig {
    public static final String API_USER_LOGIN = BuildConfig.BASE_URL
            + "api/login";
    public static final String API_USER_SIGN_UP = BuildConfig.BASE_URL
            + "api/register";


    public static final String URL_FORGET_PASSWORD = "http://dev.lara.appycms.com/password/reset";
    public static final String URL_SCHEDULING_FAQ = "file:///android_asset/html/scheduling_faq.html";
    private ApiUrlConfig() {
        // This class is not publicly instantiable
    }
}
