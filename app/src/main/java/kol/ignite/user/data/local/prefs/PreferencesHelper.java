package kol.ignite.user.data.local.prefs;

public interface PreferencesHelper {

    String getAccessToken();

    void setAccessToken(String token);

    void Logout();

    boolean isUserLoggedIn();
}
