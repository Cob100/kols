package kol.ignite.user.data.model;

public class User {
    private String fullName, username, email, phoneNumber, id;

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFullname() {
        return fullName;
    }

    public String getPhonenumber() {
        return phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFullname(String fullname) {
        this.fullName = fullname;
    }

    public void setPhonenumber(String phonenumber) {
        this.phoneNumber = phonenumber;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User(String _id, String _fullName, String _userName,
                String _email, String _phoneNumber){
        this.id=_id;
        this.fullName = _fullName;
        this.username = _userName;
        this.email = _email;
        this.phoneNumber = _phoneNumber;
    }
}
