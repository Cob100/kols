package kol.ignite.user.config;

public final class BuildConfig {

    public static final boolean DEBUG = Boolean.parseBoolean("true");
    public static final String APPLICATION_ID = "com.ignite.kol";
    public static final String BUILD_TYPE = "debug";
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "1.0";
    // Fields from build type: debug
    public static final String API_KEY = "ABCXYZ123TEST";
    public static final String BASE_URL = "http://3.17.27.147/";
}
