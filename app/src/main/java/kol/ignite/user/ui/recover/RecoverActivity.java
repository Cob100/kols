package kol.ignite.user.ui.recover;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import kol.ignite.user.BR;
import kol.ignite.user.R;
import kol.ignite.user.databinding.ActivityRecoverBinding;
import kol.ignite.user.ui.base.BaseActivity;
import kol.ignite.user.ui.login.LoginActivity;
import kol.ignite.user.utils.manager.AlertManager;

public class RecoverActivity extends BaseActivity<ActivityRecoverBinding, RecoverViewModel>
    implements RecoverNavigator{

    @Inject
    RecoverViewModel recoverViewModel;

    @Inject
    int layoutId = R.layout.activity_recover;

    ActivityRecoverBinding mBinder;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
        doBinding();
    }

    private void doBinding() {
        mBinder = getViewDataBinding();
        mBinder.setRecoverviewmodel(recoverViewModel);
        mBinder.setNavigator(this);
        getViewModel().setNavigator(this);
    }

    @Override
    public RecoverViewModel getViewModel() {
        return recoverViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.recoverviewmodel;
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void loadSubmit() {
        getViewModel().recover();
    }

    @Override
    public void setEditTextError(int i) {
        if (i == 1) {
            mBinder.email.setError("Please, enter this field");
            mBinder.email.requestFocus();
        }
    }

    @Override
    public void toastMsg(String s) {
        AlertManager alertManager = new AlertManager(this);
        alertManager.showQuickToast(s);
    }

    AlertManager alertManagerSpin;
    @Override
    public void showSpinLoader() {
        alertManagerSpin = new AlertManager(this);
        alertManagerSpin.showCustomDialog();
    }

    @Override
    public void hideSpinLoader() {
        alertManagerSpin.cancelCustomDialog();
    }

    @Override
    public void loadHome() {
        startActivity(new Intent(RecoverActivity.this, LoginActivity.class));
        finish();
    }
}
