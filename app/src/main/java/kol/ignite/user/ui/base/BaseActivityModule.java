package kol.ignite.user.ui.base;


import dagger.Module;
import dagger.Provides;
import kol.ignite.user.network.account.RetrofitInstance;

@Module
public class BaseActivityModule  {

    @Provides
    RetrofitInstance provideRetrofit(){
        return  new RetrofitInstance();
    }
}