package kol.ignite.user.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import kol.ignite.user.R;
import kol.ignite.user.network.account.response.InboxResponse;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewHolder> {

        ItemClickListener itemClickListener;
        List<InboxResponse.InboxData.Inboxes> inboxList;


        public InboxAdapter(ItemClickListener _item, List<InboxResponse.InboxData.Inboxes> _inboxList){
                this.itemClickListener = _item;
                this.inboxList = _inboxList;
        }


        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inbox, viewGroup, false);
                ViewHolder viewHolder = new ViewHolder(view);
                return viewHolder;
                }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
            ViewHolder viewHolder = holder;
            InboxResponse.InboxData.Inboxes inbox = inboxList.get(i);

            String title = inbox.getTitle();
            String details = inbox.getMessage();
            String date = inbox.getDate();
            String stringDate = convertDate(date.substring(0,10));
            viewHolder.mDate.setText(stringDate);
            viewHolder.mTitle.setText(title);
            viewHolder.mBody.setText(details);
            viewHolder.mInbox.setOnClickListener(v ->
                    itemClickListener.onItemClicked(title, details));
        }

        @Override
        public int getItemCount() {
                if(null == inboxList) return 0;
                return this.inboxList.size();

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mDate, mTitle, mBody;
            public RelativeLayout mInbox;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                mDate = itemView.findViewById(R.id.tv_date);
                mTitle = itemView.findViewById(R.id.title);
                mBody = itemView.findViewById(R.id.body);
                mInbox = itemView.findViewById(R.id.rl_inbox);
            }
        }

        public interface ItemClickListener{
            void onItemClicked(String title, String details);
        }


        public String convertDate(String serverDate){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = sdf.parse(serverDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String dateVal = date.toString().substring(4,10);
            return dateVal;

        }
}

