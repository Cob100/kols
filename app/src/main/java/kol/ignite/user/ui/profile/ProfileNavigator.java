package kol.ignite.user.ui.profile;

public interface ProfileNavigator {
    void onBack();
    void vote();
    void showSpinLoader();
    void hideSpinLoader();

    void toastMsg(String resMsg);
}
