package kol.ignite.user.ui.reset;

import android.content.Context;
import android.os.Bundle;

import javax.inject.Inject;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import kol.ignite.user.BR;
import kol.ignite.user.R;
import kol.ignite.user.databinding.ActivityResetBinding;
import kol.ignite.user.ui.base.BaseActivity;
import kol.ignite.user.utils.manager.AlertManager;
import kol.ignite.user.utils.manager.PrefManager;

public class ResetActivity extends BaseActivity<ActivityResetBinding, ResetViewModel>
    implements ResetNavigator{

    @Inject
    ResetViewModel resetViewModel;

    @Inject
    int layoutId = R.layout.activity_reset;

    ActivityResetBinding mBinder;


    public final String TOKEN = PrefManager.getInstance(ResetActivity.this).getToken();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doBinding();
    }

    private void doBinding() {
        mBinder = getViewDataBinding();
        mBinder.setNavigator(this);
        mBinder.setResetviewmodel(resetViewModel);
        getViewModel().setNavigator(this);
    }

    @Override
    public ResetViewModel getViewModel() {
        return resetViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.resetviewmodel;
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public void setEditTextError(int i) {
        switch (i){
            case 1:
                mBinder.newp.setError("Please, enter this field");
                mBinder.newp.requestFocus();
                break;
            case 2:
                mBinder.retype.setError("Please, enter this field");
                mBinder.retype.requestFocus();
                break;
        }
    }

    @Override
    public void toastMsg(String s) {
        AlertManager alertManager = new AlertManager(this);
        alertManager.showQuickToast(s);
    }

    @Override
    public void loadSubmit() {
        getViewModel().reset(TOKEN);
    }

    AlertManager alertManagerSpin;
    @Override
    public void showSpinLoader() {
        alertManagerSpin = new AlertManager(this);
        alertManagerSpin.showCustomDialog();
    }

    @Override
    public void hideSpinLoader() {
        alertManagerSpin.cancelCustomDialog();
    }

    @Override
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
