package kol.ignite.user.ui.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import kol.ignite.user.BR;
import kol.ignite.user.R;
import kol.ignite.user.databinding.ActivitySignUpBinding;
import kol.ignite.user.ui.base.BaseActivity;
import kol.ignite.user.ui.login.LoginActivity;
import kol.ignite.user.utils.manager.AlertManager;

public class SignUpActivity extends BaseActivity<ActivitySignUpBinding, SignUpViewModel>
    implements SignUpNavigator{

    @Inject
    SignUpViewModel signUpViewModel;

    @Inject
    int layoutId = R.layout.activity_sign_up;

    ActivitySignUpBinding mBinder;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doBinding();
    }

    private void doBinding() {
        mBinder = getViewDataBinding();
        mBinder.setSignviewmodel(signUpViewModel);
        mBinder.setNavigator(this);
        getViewModel().setNavigator(this);
    }

    @Override
    public SignUpViewModel getViewModel() {
        return signUpViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.signviewmodel;
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void loadSubmit() {
        getViewModel().register();
    }

    @Override
    public void setError(int state) {
        switch (state) {
            case 1:
                mBinder.fullName.setError("Please, Enter this field");
                mBinder.fullName.requestFocus();
                break;
            case 2:
                mBinder.username.setError("Please, enter this field");
                mBinder.username.requestFocus();
                break;
            case 3:
                mBinder.phonenumber.setError("Please, enter this field");
                mBinder.phonenumber.requestFocus();
                break;
            case 4:
                mBinder.email.setError("Please, enter this field");
                mBinder.email.requestFocus();
                break;
            case 5:
                mBinder.password.setError("Please, enter this field");
                mBinder.password.requestFocus();
                break;
            case 6:
                mBinder.retype.setError("Please, enter this field");
                mBinder.retype.requestFocus();
                break;
        }
    }

    @Override
    public void toastError(String value) {
        AlertManager alertManager = new AlertManager(this);
        alertManager.showQuickToast(value);
    }

    AlertManager alertManagerSpin;
    @Override
    public void showSpinLoader() {
        alertManagerSpin = new AlertManager(this);
        alertManagerSpin.showCustomDialog();
    }

    @Override
    public void hideSpinLoader() {
        alertManagerSpin.cancelCustomDialog();
    }

    @Override
    public void startLogin() {
        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
    }
}
