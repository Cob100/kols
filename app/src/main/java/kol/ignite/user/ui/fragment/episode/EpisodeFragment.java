package kol.ignite.user.ui.fragment.episode;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.todkars.shimmer.ShimmerRecyclerView;

import java.util.ArrayList;
import java.util.List;

import kol.ignite.user.R;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.VideoResponse;
import kol.ignite.user.ui.adapter.EpisodeAdapter;
import kol.ignite.user.ui.detail.DetailedActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EpisodeFragment extends Fragment {

    ShimmerRecyclerView mRv;


    public EpisodeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_episode, container, false);
        init(view);

        EpisodeAdapter.ItemClickListener itemClickListener = (id, videoUrl,list,desp, title) -> {
            List<String> commenTitleList =new ArrayList<>();
            for (int i=0; i<list.size(); i++){
                commenTitleList.add(list.get(i).getComment());
            }

            List<String> commentUsernameList =new ArrayList<>();
            for (int i=0; i<list.size(); i++){
                commentUsernameList.add(list.get(i).getUsernameComment());
            }

            Intent intent = new Intent(EpisodeFragment.this.getActivity(), DetailedActivity.class);
            intent.putExtra("id", id);
            intent.putExtra("url", videoUrl);
            intent.putExtra("desp", desp);
            intent.putExtra("title", title);
            intent.putStringArrayListExtra("comment",(ArrayList<String>) commenTitleList);
            intent.putStringArrayListExtra("username", (ArrayList<String>) commentUsernameList);
            EpisodeFragment.this.startActivity(intent);
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRv.showShimmer();
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        Call<VideoResponse> responseCall = retrofitInstance.getApi().getVideos();
        responseCall.enqueue(new Callback<VideoResponse>() {
            @Override
            public void onResponse(Call<VideoResponse> call, Response<VideoResponse> response) {
                mRv.hideShimmer();
                if (response.isSuccessful()){
                    int resCode = response.body().getStatuscode();
                    if (resCode==200){
                        List<VideoResponse.videoData.Videos> videosList = response.body().getData().getVideos();
                        RecyclerView.Adapter adapter = new EpisodeAdapter(itemClickListener, videosList, getActivity());
                        mRv.setLayoutManager(layoutManager, R.layout.episode);
                        mRv.setAdapter(adapter);
                    }else if (resCode == 400){
                        String resmsg = response.body().getMessage() + " ";
                        Log.v("erro400", resmsg);
                    }
                }else{
                    String resError = response.code() + " " + response.message();
                    Log.v("resError", resError);
                }
            }

            @Override
            public void onFailure(Call<VideoResponse> call, Throwable t) {
                mRv.hideShimmer();
                String tError = t.getMessage() + " ";
                Log.v("tError", tError);
                Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();

            }
        });

        return view;
    }

    private void init(View view) {
        mRv = view.findViewById(R.id.shimmer_recycler_view);
    }

}
