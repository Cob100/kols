package kol.ignite.user.ui.signup;

public interface SignUpNavigator {
    void onBack();
    void loadSubmit();
    void setError(int state);
    void toastError(String value);

    void showSpinLoader();

    void hideSpinLoader();

    void startLogin();
}
