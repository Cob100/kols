package kol.ignite.user.ui.detailinbox;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.rx.SchedulerProvider;

public class InboxDetailViewModel extends BaseViewModel<InboxDetailNavigator> {
    public InboxDetailViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void setNavigator(InboxDetailNavigator navigator) {
        super.setNavigator(navigator);
    }
}
