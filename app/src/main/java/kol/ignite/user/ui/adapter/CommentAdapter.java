package kol.ignite.user.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import kol.ignite.user.R;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {


        ArrayList<String> commentList;
        ArrayList<String> usernameList;

        public CommentAdapter(ArrayList<String> commentList,
                          ArrayList<String> commentUsername) {
            this.commentList = commentList;
            this.usernameList = commentUsername;
        }

        @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                    View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment, viewGroup, false);
                    ViewHolder viewHolder = new ViewHolder(view);
                    return viewHolder;
                    }

            @Override
            public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
                ViewHolder viewHolder = holder;

                String comment = commentList.get(i);
                String username = usernameList.get(i);

                String concatString = "@" + username + "\t" + comment;

                viewHolder.mCommet.setText(concatString);
            }

            @Override
            public int getItemCount() {
                    if(null == commentList) return 0;
                    return this.commentList.size();
            }

            public class ViewHolder extends RecyclerView.ViewHolder {
                public TextView mCommet;
                public ViewHolder(@NonNull View itemView) {
                    super(itemView);
                    mCommet = itemView.findViewById(R.id.text_comment);
                }
            }

}

