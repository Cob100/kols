package kol.ignite.user.ui.stream;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.R;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class StreamActivityModule {
    @Provides
    StreamViewModel provideStreamViemModel(DataManager dataManager, SchedulerProvider schedulerProvider){
        return new StreamViewModel(dataManager, schedulerProvider);
    }

    @Provides
    int providesLayoutId(){
        return R.layout.activity_stream;
    }
}
