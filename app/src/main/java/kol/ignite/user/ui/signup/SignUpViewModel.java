package kol.ignite.user.ui.signup;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableField;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.SignupResponse;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.helper.ValidationUtils;
import kol.ignite.user.utils.rx.SchedulerProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpViewModel extends BaseViewModel<SignUpNavigator> {

    public ObservableField<String> fullName = new ObservableField<>("");
    public ObservableField<String> username = new ObservableField<>("");
    public ObservableField<String> phonenumber = new ObservableField<>("");
    public ObservableField<String> email = new ObservableField<>("");
    public ObservableField<String> password = new ObservableField<>("");
    public ObservableField<String> retype = new ObservableField<>("");


    SignUpViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void setNavigator(SignUpNavigator navigator) {
        super.setNavigator(navigator);
    }

    void register(){
        String mFullname = fullName.get();
        String mUsername = username.get();
        String mPhonenumber = phonenumber.get();
        String mEmail = email.get();
        String mPassword = password.get();
        String mRetype = retype.get();

        if (TextUtils.isEmpty(mFullname)){
            getNavigator().setError(1);
            return;
        }
        if (TextUtils.isEmpty(mUsername)){
            getNavigator().setError(2);
            return;
        }
        if (TextUtils.isEmpty(mPhonenumber)){
            getNavigator().setError(3);
            return;
        }
        if (TextUtils.isEmpty(mEmail)){
            getNavigator().setError(4);
            return;
        }
        if (TextUtils.isEmpty(mPassword)){
            getNavigator().setError(5);
            return;
        }
        if (TextUtils.isEmpty(mRetype)){
            getNavigator().setError(6);
            return;
        }

        if (mPassword.equals(mRetype)){
            if (ValidationUtils.isEmailValid(mEmail)){
                if (ValidationUtils.isPasswordValid(mPassword)){
                    getNavigator().showSpinLoader();

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("fullname", mFullname);
                    jsonObject.addProperty("username", mUsername);
                    jsonObject.addProperty("email", mEmail);
                    jsonObject.addProperty("phonenumber", mPhonenumber);
                    jsonObject.addProperty("password", mPassword);

                    RetrofitInstance retrofitInstance = new RetrofitInstance();
                    Call<SignupResponse> responseCall = retrofitInstance.getApi().reg(jsonObject);
                    responseCall.enqueue(new Callback<SignupResponse>() {
                        @Override
                        public void onResponse(@NotNull Call<SignupResponse> call,
                                               @NotNull Response<SignupResponse> response) {
                            getNavigator().hideSpinLoader();
                            if (response.isSuccessful()){
                                assert response.body() != null;
                                int resCode = response.body().getStatuscode();
                                if (resCode ==200){
                                    getNavigator().toastError(response.body().getMessage());
                                    getNavigator().startLogin();
                                }else if (resCode == 400){
                                    getNavigator().toastError(response.body().getMessage());
                                }
                            }
                        }
                        @Override
                        public void onFailure(@NotNull Call<SignupResponse> call, @NotNull Throwable t) {
                            getNavigator().hideSpinLoader();
                            String tError = t.getMessage() + " ";
                            Log.v("tError", tError);
                            getNavigator().toastError("Please check your internet connection");
                        }
                    });
                }else{
                    getNavigator().toastError("Password must be more than 6 character");
                }
            }else {
                getNavigator().toastError("Invalid Email");
            }


        }else {
            getNavigator().toastError("Password does not match");
        }




    }
}
