package kol.ignite.user.ui.profile;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import com.bumptech.glide.Glide;

import javax.inject.Inject;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import kol.ignite.user.BR;
import kol.ignite.user.R;
import kol.ignite.user.databinding.ActivityProfileBinding;
import kol.ignite.user.ui.base.BaseActivity;
import kol.ignite.user.utils.manager.AlertManager;
import kol.ignite.user.utils.manager.PrefManager;

public class ProfileActivity extends BaseActivity<ActivityProfileBinding, ProfileViewModel>
    implements ProfileNavigator{

    @Inject
    ProfileViewModel profileViewModel;

    @Inject
    int layoutId = R.layout.activity_profile;

    ActivityProfileBinding mBinder;
    String influencerId;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doBinding();
    }

    private void doBinding() {
        mBinder = getViewDataBinding();
        mBinder.setProfileviewmodel(profileViewModel);
        mBinder.setNavigator(this);
        getViewModel().setNavigator(this);
        init();
    }

    private void init() {
        String fullname = getIntent().getStringExtra("fullname");
        String username = getIntent().getStringExtra("username");
        String url = getIntent().getStringExtra("profileUrl");
        influencerId = getIntent().getStringExtra("id");
        mBinder.tvFullname.setText(fullname);
        mBinder.tvUsername.setText(username);
        Glide.with(this).load(url).into(mBinder.profileImage);


    }

    @Override
    public ProfileViewModel getViewModel() {
        return profileViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.profileviewmodel;
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void vote() {
        String TOKEN = PrefManager.getInstance(ProfileActivity.this).getToken();
        getViewModel().callInfluencerVote(influencerId, TOKEN);
    }

    AlertManager alertManager;
    @Override
    public void showSpinLoader() {
        alertManager = new AlertManager(this);
        alertManager.showCustomDialog();
    }

    @Override
    public void hideSpinLoader() {
        alertManager.cancelCustomDialog();
    }

    @Override
    public void toastMsg(String s) {
        AlertManager alertManager = new AlertManager(this);
        alertManager.showQuickToast(s);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
