package kol.ignite.user.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import kol.ignite.user.R;

public class FavEpisodeAdapter extends RecyclerView.Adapter<FavEpisodeAdapter.ViewHolder> {

        ItemClickListener itemClickListener;

        String[] names = {"Episode 1"};

        public FavEpisodeAdapter(ItemClickListener _item){
                itemClickListener = _item;
        }


        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.episode, viewGroup, false);
                ViewHolder viewHolder = new ViewHolder(view);
                return viewHolder;
                }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
            ViewHolder viewHolder = holder;

            viewHolder.mAddressBox.setText(names[i]);

            viewHolder.mAlert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClicked();
                }
            });
        }

        @Override
        public int getItemCount() {
                if(null == names) return 0;
                return this.names.length;
                }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mAddressBox;
            public FrameLayout mAlert;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                mAddressBox = itemView.findViewById(R.id.title);
                mAlert = itemView.findViewById(R.id.fl_episode);
            }
        }

        public interface ItemClickListener{
            void onItemClicked();
        }
}

