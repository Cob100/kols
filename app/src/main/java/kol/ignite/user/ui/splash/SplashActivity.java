package kol.ignite.user.ui.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import kol.ignite.user.R;
import kol.ignite.user.ui.home.MainActivity;
import kol.ignite.user.ui.loading.LoadingActivity;
import kol.ignite.user.utils.manager.PrefManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(() -> {
            if (PrefManager.getInstance(this).getToken()!=null){
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }else{
                startActivity(new Intent(SplashActivity.this, LoadingActivity.class));
                finish();
            }

        }, 2000);
    }
}
