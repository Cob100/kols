package kol.ignite.user.ui.home;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import kol.ignite.user.BR;
import kol.ignite.user.R;
import kol.ignite.user.databinding.ActivityMainBinding;
import kol.ignite.user.ui.base.BaseActivity;
import kol.ignite.user.ui.fragment.account.AccountFragment;
import kol.ignite.user.ui.fragment.episode.EpisodeFragment;
import kol.ignite.user.ui.fragment.homefrag.HomeFragment;
import kol.ignite.user.ui.fragment.inbox.InboxFragment;
import kol.ignite.user.ui.fragment.votefrag.VoteFragment;
import kol.ignite.user.utils.manager.PrefManager;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel>
        implements MainNavigator {

    @Inject
    int layoutId = R.layout.activity_main;

    @Inject
    MainViewModel mainViewModel;

    String TOKEN = PrefManager.getInstance(MainActivity.this).getToken();


    HomeFragment homeFragment = new HomeFragment();
    InboxFragment inboxFragment = new InboxFragment(TOKEN);
    VoteFragment voteFragment = new VoteFragment();
    EpisodeFragment episodeFragment = new EpisodeFragment();
    AccountFragment accountFragment = new AccountFragment();

    ActivityMainBinding mBinder;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doBinding();
    }

    private void doBinding(){
        mBinder = getViewDataBinding();
        mBinder.setMainViewModel(mainViewModel);
        mainViewModel.setNavigator(this);
        getViewModel().setNavigator(this);
        resetTabs();
        replaceFragment(1);
    }




    @Override
    public MainViewModel getViewModel() {
        //mainViewModel = ViewModelProviders.of(this, mViewModelFactory).get(MainViewModel.class);
        return mainViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.mainViewModel;
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public void replaceFragment(int i) {
        if (i==1){
            mBinder.homeImage.setImageResource(R.drawable.ic_home_colored);
            mBinder.homeText.setTextColor(getResources().getColor(R.color.colored));
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(mBinder.container.getId(), homeFragment);
            transaction.commit();
        } else if (i == 2){
            mBinder.episodeImage.setImageResource(R.drawable.ic_episodes_colored);
            mBinder.episodeText.setTextColor(getResources().getColor(R.color.colored));
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(mBinder.container.getId(), episodeFragment);
            transaction.commit();
        } else if (i ==3){
            mBinder.voteImage.setImageResource(R.drawable.ic_vote);
            mBinder.voteText.setTextColor(getResources().getColor(R.color.colored));
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(mBinder.container.getId(), voteFragment);
            transaction.commit();
        } else if (i ==4){
            mBinder.inboxImage.setImageResource(R.drawable.ic_inbox_colored);
            mBinder.inboxText.setTextColor(getResources().getColor(R.color.colored));
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(mBinder.container.getId(), inboxFragment);
            transaction.commit();
        } else if (i ==5){
            mBinder.accountImage.setImageResource(R.drawable.ic_account_colored);
            mBinder.accountText.setTextColor(getResources().getColor(R.color.colored));
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(mBinder.container.getId(), accountFragment);
            transaction.commit();
        }
    }

    public void setProfileMenu(){
        resetTabs();
        mBinder.accountImage.setImageResource(R.drawable.ic_account_colored);
        mBinder.accountText.setTextColor(getResources().getColor(R.color.colored));
    }


    @Override
    public void resetTabs() {
        mBinder.homeImage.setImageResource(R.drawable.ic_home_gray);
        mBinder.episodeImage.setImageResource(R.drawable.ic_episodes);
        mBinder.voteImage.setImageResource(R.drawable.ic_vote);
        mBinder.inboxImage.setImageResource(R.drawable.ic_inbox);
        mBinder.accountImage.setImageResource(R.drawable.ic_account);

        mBinder.homeText.setTextColor(getResources().getColor(R.color.gray));
        mBinder.episodeText.setTextColor(getResources().getColor(R.color.gray));
        mBinder.voteText.setTextColor(getResources().getColor(R.color.gray));
        mBinder.inboxText.setTextColor(getResources().getColor(R.color.gray));
        mBinder.accountText.setTextColor(getResources().getColor(R.color.gray));

    }


}
