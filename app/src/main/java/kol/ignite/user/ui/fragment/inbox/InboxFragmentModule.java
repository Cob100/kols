package kol.ignite.user.ui.fragment.inbox;

import androidx.lifecycle.ViewModelProvider;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.R;
import kol.ignite.user.ViewModelProviderFactory;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class InboxFragmentModule {
    @Provides
    InboxViewModel provideInboxViewModel(DataManager dataManager,
                                                 SchedulerProvider schedulerProvider) {
        return new InboxViewModel(dataManager, schedulerProvider);
    }

    @Provides
    ViewModelProvider.Factory inboxViewModelProvider(InboxViewModel inboxViewModel) {
        return new ViewModelProviderFactory<>(inboxViewModel);
    }

    @Provides
    int provideLayoutId() {
        return R.layout.fragment_inbox;
    }

}
