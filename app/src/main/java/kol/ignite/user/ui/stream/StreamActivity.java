package kol.ignite.user.ui.stream;


import android.content.Context;
import android.os.Bundle;

import javax.inject.Inject;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import kol.ignite.user.BR;
import kol.ignite.user.R;
import kol.ignite.user.databinding.ActivityStreamBinding;
import kol.ignite.user.ui.base.BaseActivity;

public class StreamActivity extends BaseActivity<ActivityStreamBinding,
        StreamViewModel> implements StreamNavigator {

    @Inject
    StreamViewModel streamViewModel;

    @Inject
    int layoutId = R.layout.activity_stream;

    ActivityStreamBinding mBinder;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());*/
        doBinding();
    }

    private void doBinding() {
        mBinder = getViewDataBinding();
        mBinder.setStreamviewmodel(streamViewModel);
        mBinder.setNavigator(this);
        getViewModel().setNavigator(this);
    }

    @Override
    public StreamViewModel getViewModel() {
        return streamViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.streamviewmodel;
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }
}
