package kol.ignite.user.ui.fragment.inbox;

import java.util.List;

import kol.ignite.user.network.account.response.InboxResponse;

public interface InboxNavigator {
    void showSpinLoader();
    void hideSpinLoader();
    void toastMsg(String s);

    void setInboxList(List<InboxResponse.InboxData.Inboxes> inboxList);
}
