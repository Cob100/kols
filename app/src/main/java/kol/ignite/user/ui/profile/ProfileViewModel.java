package kol.ignite.user.ui.profile;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.DefaultResponse;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.manager.AlertManager;
import kol.ignite.user.utils.manager.PrefManager;
import kol.ignite.user.utils.rx.SchedulerProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileViewModel extends BaseViewModel<ProfileNavigator> {
    public ProfileViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void setNavigator(ProfileNavigator navigator) {
        super.setNavigator(navigator);
    }

    void callInfluencerVote(String id, String TOKEN) {
        getNavigator().showSpinLoader();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("influencerid", id);


        RetrofitInstance retrofitInstance = new RetrofitInstance();
        Call<DefaultResponse> responseCall = retrofitInstance.getApi().voteInfluencer(TOKEN, jsonObject);
        responseCall.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                getNavigator().hideSpinLoader();
                if (response.isSuccessful()){
                    int resCode = response.body().getStatuscode();
                    if (resCode == 200){
                        String resMsg = response.body().getMessage()+" ";
                        getNavigator().toastMsg(resMsg);
                    }else if(resCode == 400){
                        String resMsg = response.body().getMessage() + " ";
                        getNavigator().toastMsg(resMsg);
                    }
                }else{
                    String resError = response.code() + " " + response.body().getMessage();
                    Log.v("resError", resError);
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                getNavigator().hideSpinLoader();
                String tError = t.getMessage() + " ";
                Log.v("tError", tError);
                getNavigator().toastMsg("Please check your internet connection");
            }
        });
    }
}
