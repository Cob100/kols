package kol.ignite.user.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import kol.ignite.user.AppConstants;
import kol.ignite.user.R;
import kol.ignite.user.network.account.response.VideoResponse;

public class EpisodeAdapter extends RecyclerView.Adapter<EpisodeAdapter.ViewHolder> {

        ItemClickListener itemClickListener;
        List<VideoResponse.videoData.Videos> videosList;
        Context context;

        public EpisodeAdapter(ItemClickListener _item,
                              List<VideoResponse.videoData.Videos> _videosList,
                              Context _context){
                this.itemClickListener = _item;
                this.videosList = _videosList;
                this.context = _context;
        }


        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.episode, viewGroup, false);
                ViewHolder viewHolder = new ViewHolder(view);
                return viewHolder;
                }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
            ViewHolder viewHolder = holder;

            VideoResponse.videoData.Videos videos = videosList.get(i);

            String id = videos.getId();
            String title  = videos.getTitle();
            String desp = videos.getDesp();
            String url = videos.getVideoUrl();

            List<VideoResponse.videoData.Videos.Comments> listOfComment = videos.getComments();

            String urlID = AppConstants.extractYTId(url);
            Log.v("snapUrl", url+" "+urlID);

            String snapUrl = AppConstants.snapShotUrl(urlID);
            Log.v("snapUrl", snapUrl);

            Glide.with(context).load(snapUrl).into(viewHolder.mSnapShot);
            viewHolder.mTitle.setText(title);
            viewHolder.mDesc.setText(desp);


            viewHolder.mAlert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClicked(
                            id, urlID, listOfComment, desp, title
                    );
                }
            });
        }

        @Override
        public int getItemCount() {
                if(null == videosList) return 0;
                return this.videosList.size();
                }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mTitle, mDesc;
            public ImageView mSnapShot;
            public FrameLayout mAlert;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                mTitle = itemView.findViewById(R.id.title);
                mDesc = itemView.findViewById(R.id.tv_desp);
                mSnapShot = itemView.findViewById(R.id.iv_snapshot);
                mAlert = itemView.findViewById(R.id.fl_episode);
            }
        }

        public interface ItemClickListener{
            void onItemClicked(String id, String videoUrl,
                               List<VideoResponse.videoData.Videos.Comments> listOfComment,
                               String desp, String title);
        }
}

