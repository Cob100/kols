package kol.ignite.user.ui.loading;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import kol.ignite.user.BR;
import kol.ignite.user.R;
import kol.ignite.user.databinding.ActivityLoadingBinding;
import kol.ignite.user.ui.base.BaseActivity;
import kol.ignite.user.ui.login.LoginActivity;
import kol.ignite.user.ui.signup.SignUpActivity;

public class LoadingActivity extends BaseActivity<
        ActivityLoadingBinding, LoadingViewModel> implements LoadingNavigator{

    @Inject
    LoadingViewModel loadingViewModel;

    @Inject
    int layoutId = R.layout.activity_loading;

    ActivityLoadingBinding mBinder;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
        doBinding();
    }

    private void doBinding() {
        mBinder = getViewDataBinding();
        mBinder.setLoadingviewmodel(loadingViewModel);
        mBinder.setNavigator(this);
        getViewModel().setNavigator(this);
    }

    @Override
    public LoadingViewModel getViewModel() {
        return loadingViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.loadingviewmodel;
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public void loadSignIn() {
        startActivity(new Intent(LoadingActivity.this, LoginActivity.class));
    }

    @Override
    public void loadRegister() {
        startActivity(new Intent(LoadingActivity.this, SignUpActivity.class));

    }
}
