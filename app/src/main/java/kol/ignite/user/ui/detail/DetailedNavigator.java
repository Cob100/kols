package kol.ignite.user.ui.detail;

public interface DetailedNavigator {
    void onBack();
    void submitComment();

    void toastMsg(String s);

    void showSpinLoader();

    void hideSpinLoader();
}
