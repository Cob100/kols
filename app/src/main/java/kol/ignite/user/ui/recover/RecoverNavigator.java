package kol.ignite.user.ui.recover;

public interface RecoverNavigator {
    void onBack();
    void loadSubmit();
    void setEditTextError(int i);
    void toastMsg(String s);
    void showSpinLoader();
    void hideSpinLoader();

    void loadHome();
}
