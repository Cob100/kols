package kol.ignite.user.ui.fragment.account;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import kol.ignite.user.R;
import kol.ignite.user.data.local.prefs.AppPreferencesHelper;
import kol.ignite.user.data.model.User;
import kol.ignite.user.ui.adapter.FavEpisodeAdapter;
import kol.ignite.user.ui.adapter.FavInfluAdapter;
import kol.ignite.user.ui.detail.DetailedActivity;
import kol.ignite.user.ui.loading.LoadingActivity;
import kol.ignite.user.ui.reset.ResetActivity;
import kol.ignite.user.utils.manager.PrefManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        TextView mFullname = view.findViewById(R.id.tv_fullname);
        TextView mUsername = view.findViewById(R.id.tv_username);
        TextView mEmail = view.findViewById(R.id.tv_email);

        User user = AppPreferencesHelper.getInstance(getActivity()).getUser();

        mFullname.setText(user.getFullname());
        String va = user.getUsername();
        mUsername.setText(va);
        mEmail.setText(user.getEmail());

        RecyclerView recyclerView = view.findViewById(R.id.rv_favorite);
        LinearLayoutManager linearLayoutManager =  new LinearLayoutManager(getActivity());
        FavEpisodeAdapter.ItemClickListener itemClickListener = () -> {
            //startActivity(new Intent(AccountFragment.this.getActivity(), DetailedActivity.class));
        };

        RecyclerView.Adapter adapter = new FavEpisodeAdapter(itemClickListener);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        RecyclerView recyclerViewInflu = view.findViewById(R.id.rv_favorite_influ);
        LinearLayoutManager linearLayoutManagerInflu =  new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        FavInfluAdapter.ItemClickListener itemClickListenerInflu = () ->{

        };

        RecyclerView.Adapter adapterInflu = new FavInfluAdapter(itemClickListenerInflu);

        recyclerViewInflu.setLayoutManager(linearLayoutManagerInflu);
        recyclerViewInflu.setAdapter(adapterInflu);


        FrameLayout resetLayout = view.findViewById(R.id.fl_reset_password);
        resetLayout.setOnClickListener(v -> startActivity(new Intent(getActivity(), ResetActivity.class)));

        FrameLayout logout = view.findViewById(R.id.fl_logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefManager.getInstance(getActivity()).logout();
                startActivity(new Intent(getActivity(), LoadingActivity.class));
                getActivity().finish();
            }
        });

        return view;
    }

}
