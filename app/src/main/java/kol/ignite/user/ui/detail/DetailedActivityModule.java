package kol.ignite.user.ui.detail;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.R;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class DetailedActivityModule {
    @Provides
    DetailedViewModel provideDetailedViewModel(DataManager dataManager,
                                               SchedulerProvider schedulerProvider){
        return new DetailedViewModel(dataManager, schedulerProvider);
    }

    @Provides
    int provideLayoutId(){
        return R.layout.activity_detailed;
    }
}
