package kol.ignite.user.ui.reset;

public interface ResetNavigator {
    void setEditTextError(int i);
    void toastMsg(String s);
    void loadSubmit();
    void showSpinLoader();
    void hideSpinLoader();
    void onBack();
}
