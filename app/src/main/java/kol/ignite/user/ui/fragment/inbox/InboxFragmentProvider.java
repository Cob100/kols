package kol.ignite.user.ui.fragment.inbox;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class InboxFragmentProvider {

    @ContributesAndroidInjector(modules = InboxFragmentModule.class)
    abstract InboxFragment provideInboxFragmentFactory();
}
