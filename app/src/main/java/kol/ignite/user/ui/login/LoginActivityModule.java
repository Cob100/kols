package kol.ignite.user.ui.login;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.R;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class LoginActivityModule {

    @Provides
    LoginViewModel provideLoginViewModel(DataManager dataManager,
                                         SchedulerProvider schedulerProvider){
        return new LoginViewModel(dataManager, schedulerProvider);
    }

    @Provides
    int provideLayoutId(){
        return R.layout.activity_login;
    }
}
