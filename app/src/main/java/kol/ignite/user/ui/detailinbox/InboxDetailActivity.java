package kol.ignite.user.ui.detailinbox;

import android.content.Context;
import android.os.Bundle;

import javax.inject.Inject;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import kol.ignite.user.BR;
import kol.ignite.user.R;
import kol.ignite.user.databinding.ActivityInboxDetailBinding;
import kol.ignite.user.ui.base.BaseActivity;

public class InboxDetailActivity extends BaseActivity<ActivityInboxDetailBinding, InboxDetailViewModel>
    implements InboxDetailNavigator{

    @Inject
    InboxDetailViewModel inboxDetailViewModel;

    @Inject
    int layoutId = R.layout.activity_inbox_detail;

    ActivityInboxDetailBinding mBinder;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doBinding();
    }

    private void doBinding() {
        mBinder = getViewDataBinding();
        mBinder.setInboxdetailviewmodel(inboxDetailViewModel);
        mBinder.setNavigator(this);
        getViewModel().setNavigator(this);

        init();
    }

    private void init() {
        String title = getIntent().getStringExtra("title");
        String details = getIntent().getStringExtra("details");

        mBinder.tvTitle.setText(title);
        mBinder.tvDetails.setText(details);
    }

    @Override
    public InboxDetailViewModel getViewModel() {
        return inboxDetailViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.inboxdetailviewmodel;
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
