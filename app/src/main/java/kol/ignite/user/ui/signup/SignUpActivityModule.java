package kol.ignite.user.ui.signup;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.R;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class SignUpActivityModule {
    @Provides
    SignUpViewModel provideSignUpViewModel(DataManager dataManager,
                                           SchedulerProvider schedulerProvider){
        return new SignUpViewModel(dataManager, schedulerProvider);
    }

    @Provides
    int provideLayoutId(){
        return R.layout.activity_sign_up;
    }
}
