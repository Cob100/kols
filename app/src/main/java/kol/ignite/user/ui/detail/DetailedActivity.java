package kol.ignite.user.ui.detail;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import javax.inject.Inject;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.annotations.NonNull;
import kol.ignite.user.BR;
import kol.ignite.user.R;
import kol.ignite.user.data.local.prefs.AppPreferencesHelper;
import kol.ignite.user.databinding.ActivityDetailedBinding;
import kol.ignite.user.ui.adapter.CommentAdapter;
import kol.ignite.user.ui.base.BaseActivity;
import kol.ignite.user.utils.manager.AlertManager;
import kol.ignite.user.utils.manager.PrefManager;

public class DetailedActivity extends BaseActivity<ActivityDetailedBinding, DetailedViewModel>
    implements DetailedNavigator{

    @Inject
    DetailedViewModel detailedViewModel;

    @Inject
    int layoutId = R.layout.activity_detailed;

    ActivityDetailedBinding mBinder;

    String id;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doBinding();
    }

    private void doBinding() {
        mBinder = getViewDataBinding();
        mBinder.setDetailviewmodel(detailedViewModel);
        mBinder.setNavigator(this);
        getViewModel().setNavigator(this);

        init();
    }

    private void init() {
        id = getIntent().getStringExtra("id");
        String url = getIntent().getStringExtra("url");
        String desp = getIntent().getStringExtra("desp");
        String title = getIntent().getStringExtra("title");
        ArrayList<String> commentList = getIntent().getStringArrayListExtra("comment");
        ArrayList<String> commentUsername = getIntent().getStringArrayListExtra("username");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.Adapter adapter = new CommentAdapter(commentList, commentUsername);
        mBinder.rvComment.setLayoutManager(layoutManager);
        mBinder.rvComment.setAdapter(adapter);
        mBinder.desp.setText(desp);
        mBinder.title.setText(title);
        startPlaying(url);
    }

    void startPlaying(String url) {
        getLifecycle().addObserver(mBinder.youtubePlayerView);
        mBinder.youtubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NotNull @NonNull YouTubePlayer youTubePlayer) {
                youTubePlayer.loadVideo(url, 0f);
            }
        });
    }


    @Override
    public DetailedViewModel getViewModel() {
        return detailedViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.detailviewmodel;
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void submitComment() {

        String TOKEN = PrefManager.getInstance(this).getToken();
        String username = AppPreferencesHelper.getInstance(this).getUser().getUsername();

        getViewModel().comment(TOKEN, username, id);
    }

    @Override
    public void toastMsg(String s) {
        AlertManager manager = new AlertManager(DetailedActivity.this);
        manager.showQuickToast(s);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    AlertManager alertManagerSpin;
    @Override
    public void showSpinLoader() {
        alertManagerSpin = new AlertManager(this);
        alertManagerSpin.showCustomDialog();
    }

    @Override
    public void hideSpinLoader() {
        alertManagerSpin.cancelCustomDialog();
    }
}
