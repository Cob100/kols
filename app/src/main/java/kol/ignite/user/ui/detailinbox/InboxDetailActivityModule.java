package kol.ignite.user.ui.detailinbox;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.R;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class InboxDetailActivityModule {
    @Provides
    InboxDetailViewModel provideInboxDetailViewModel(DataManager dataManager,
                                                     SchedulerProvider schedulerProvider){
        return new InboxDetailViewModel(dataManager, schedulerProvider);
    }

    @Provides
    int provideLayoutId(){
        return R.layout.activity_inbox_detail;
    }
}
