package kol.ignite.user.ui.loading;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.rx.SchedulerProvider;

public class LoadingViewModel extends BaseViewModel<LoadingNavigator> {
    public LoadingViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
    @Override
    public void setNavigator(LoadingNavigator navigator) {
        super.setNavigator(navigator);
    }
}
