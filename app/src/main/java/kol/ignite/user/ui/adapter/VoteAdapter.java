package kol.ignite.user.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import kol.ignite.user.R;
import kol.ignite.user.network.account.response.LeadershipResponse;

import static kol.ignite.user.AppConstants.PROFILE_URL;

public class VoteAdapter extends RecyclerView.Adapter<VoteAdapter.ViewHolder> {

        ItemClickListener itemClickListener;
        List<LeadershipResponse.LData.LeadershipBoard> boardList;
        Context context;

        public VoteAdapter(ItemClickListener _item,
                           List<LeadershipResponse.LData.LeadershipBoard> leadershipBoardList,
                           Context _context){
                this.itemClickListener = _item;
                this.boardList = leadershipBoardList;
                this.context = _context;
        }


        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vote, viewGroup, false);
                ViewHolder viewHolder = new ViewHolder(view);
                return viewHolder;
                }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
            ViewHolder viewHolder = holder;

            LeadershipResponse.LData.LeadershipBoard leadershipBoard = boardList.get(i);
            LeadershipResponse.LData.LeadershipBoard.Influencer leader = leadershipBoard.getInfluencers().get(0);

            String influencerId = leader.getInfluencerId();

            viewHolder.mViewProfile.setVisibility(View.VISIBLE);
            viewHolder.mOnlineVote.setVisibility(View.VISIBLE);
            viewHolder.mBio.setVisibility(View.VISIBLE);
            String fullname = leader.getFullName();


            int positionInt = leadershipBoard.getPosition();

            if (positionInt == 1){
                viewHolder.mAward.setVisibility(View.VISIBLE);
            }else{
                viewHolder.mAward.setVisibility(View.GONE);
            }

            String position = getPositionConverter(positionInt);
            viewHolder.mRanking.setText(position);

            String votesNumber = leadershipBoard.getTotalPoint() + " votes";
            viewHolder.mVote.setText(votesNumber);

            String userName = "@" + leader.getUsername();
            viewHolder.mUsername.setText(userName);

            viewHolder.mFullname.setText(fullname);

            /***************
             * we dont have*
             * bio yet******
             ***************/
            String url = PROFILE_URL + leader.getProfileImage();
            Glide.with(context).load(url).into(viewHolder.mProfileImage);

            viewHolder.mOnlineVote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //itemClickListener.vote(influencerId);
                    itemClickListener.onItemClicked(fullname,
                            userName, url, influencerId);
                }
            });


            viewHolder.mViewProfile.setOnClickListener(v ->
                    itemClickListener.onItemClicked(fullname,
                            userName, url, influencerId));
        }

        private String getPositionConverter(int position) {
            String stringPostion = "";
            switch (position){
                case 1:
                    stringPostion = "1st";
                    break;
                case 2:
                    stringPostion = "2nd";
                    break;
                case 3:
                    stringPostion = "3rd";
                    break;
                case 4:
                    stringPostion = "4th";
                    break;
                case 5:
                    stringPostion = "5th";
                    break;
                case 6:
                    stringPostion = "6th";
                    break;
                case 7:
                    stringPostion = "7th";
                    break;
                case 8:
                    stringPostion = "8th";
                    break;
                case 9:
                    stringPostion = "9th";
                    break;
                case 10:
                    stringPostion = "10th";
                    break;
            }
            return stringPostion;
    }

        @Override
            public int getItemCount() {
                    if(null == boardList) return 0;
                    return this.boardList.size();
                    }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mRanking, mFullname, mUsername, mBio, mVote, mOnlineVote;
            public ImageView mProfileImage, mAward;
            public TextView mViewProfile;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                mRanking = itemView.findViewById(R.id.ranking);
                mFullname = itemView.findViewById(R.id.fullname);
                mUsername = itemView.findViewById(R.id.username);
                mBio = itemView.findViewById(R.id.bio);
                mVote = itemView.findViewById(R.id.votes);
                mOnlineVote = itemView.findViewById(R.id.tv_vote);
                mProfileImage = itemView.findViewById(R.id.profile_image);
                mAward = itemView.findViewById(R.id.iv_award);
                mViewProfile = itemView.findViewById(R.id.tv_view_profile);
            }
        }

        public interface ItemClickListener{
            void onItemClicked(String fullname, String username,
                String url, String id);

            void vote(String id);
        }


}

