package kol.ignite.user.ui.home;

public interface MainNavigator {

    void replaceFragment(int i);

    void resetTabs();
}
