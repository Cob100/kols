package kol.ignite.user.ui.fragment.homefrag;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.annotations.NonNull;
import kol.ignite.user.AppConstants;
import kol.ignite.user.R;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.HomeResponse;
import kol.ignite.user.network.account.response.LeaderResponse;
import kol.ignite.user.network.account.response.VideoResponse;
import kol.ignite.user.network.account.response.model.LiveNow;
import kol.ignite.user.network.account.response.model.ShowingNow;
import kol.ignite.user.network.account.response.model.TopInfluencer;
import kol.ignite.user.network.account.response.model.TopTeam;
import kol.ignite.user.ui.adapter.LiveNowAdapter;
import kol.ignite.user.ui.adapter.TrendingAdapter;
import kol.ignite.user.ui.fragment.account.AccountFragment;
import kol.ignite.user.ui.fragment.episode.EpisodeFragment;
import kol.ignite.user.ui.home.MainActivity;
import kol.ignite.user.ui.stream.StreamActivity;
import kol.ignite.user.utils.manager.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    RecyclerView mRvLiveNow, mRvTrending;
    LinearLayoutManager mLinearLN, mLinearTr;
    RecyclerView.Adapter mLiveNowAdapter, mTrendingAdapter;

    CircleImageView circleImageView;

    LinearLayout liveNow;
    LiveNowAdapter.ItemClickListener itemClickListener;

    YouTubePlayerView youTubePlayerView;
    ImageView nowShowing;
    CircleImageView ttImageView;
    TextView ttFullname, ttUsername, ttVote, btnViewall, btnViewLeaderboard;

    public HomeFragment() {
        // Required empty public constructor
    }
    String token = PrefManager.getInstance(getActivity()).getToken();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        init(view);



        youTubePlayerView = view.findViewById(R.id.youtube_player_view);
        nowShowing = view.findViewById(R.id.previewImage);
        ttFullname = view.findViewById(R.id.tt_fullname);
        ttUsername = view.findViewById(R.id.tt_username);
        ttVote = view.findViewById(R.id.tt_vote);
        ttImageView = view.findViewById(R.id.tt_image);
        btnViewall = view.findViewById(R.id.btnViewall);
        btnViewLeaderboard = view.findViewById(R.id.btnViewleadershipboard);
        liveNow = view.findViewById(R.id.livenow);
        // If auto-start is set to false
        itemClickListener = () -> startActivity(new Intent(getActivity(), StreamActivity.class));

        mLinearLN = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false);

        mLinearTr = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false);

        mTrendingAdapter = new TrendingAdapter();

        mRvTrending.setLayoutManager(mLinearTr);
        mRvTrending.setAdapter(mTrendingAdapter);

        circleImageView.setOnClickListener(v -> {
            AccountFragment accountFragment = new AccountFragment();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, accountFragment);
            transaction.commit();
            ((MainActivity)getActivity()).setProfileMenu();
        });

        nowShowing.setOnClickListener(v -> {
            EpisodeFragment accountFragment = new EpisodeFragment();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, accountFragment);
            transaction.commit();
           // ((MainActivity)getActivity()).setProfileMenu();
        });

        btnViewall.setOnClickListener(v -> {
            EpisodeFragment accountFragment = new EpisodeFragment();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, accountFragment);
            transaction.commit();
            // ((MainActivity)getActivity()).setProfileMenu();
        });


        btnViewLeaderboard.setOnClickListener(v -> {
            EpisodeFragment accountFragment = new EpisodeFragment();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, accountFragment);
            transaction.commit();
            // ((MainActivity)getActivity()).setProfileMenu();
        });
        callHome();

        return view;
    }

    private void callHome() {
        String auth = "Bearer " + token;

        RetrofitInstance retrofitInstance = new RetrofitInstance();
        Call<HomeResponse> responseCall = retrofitInstance.getApi().getHome(auth);
        responseCall.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                if (response.isSuccessful()){
                    int resCode = response.body().getStatusCode();
                    if (resCode==200){
                        Log.v("erro4003", "200");
                        HomeResponse res = response.body();
                        /**************
                         *For Live now*
                         **************/
                        List<LiveNow> liveNowList = res.getData().getLiveNowList();
                        if (liveNowList.isEmpty()){
                            liveNow.setVisibility(View.GONE);
                        }else{
                            mLiveNowAdapter = new LiveNowAdapter(itemClickListener, liveNowList);
                            mRvLiveNow.setLayoutManager(mLinearLN);
                            mRvLiveNow.setAdapter(mLiveNowAdapter);
                        }
                        /*****************
                         *For Showing now*
                         *****************/
                        VideoResponse.videoData.Videos showingNow = res.getData().getShowingNowList().get(0);
                        Log.e("open preview",showingNow.hightlightimage);
                      //  String imageUrl = showingNow.getVideoUrl();
                        LoadPreviewImage(showingNow.hightlightimage);;
                      //  callLoadVideoUrl(videoUrl);

                        /***************
                         *For Top Influ*
                         ***************/
                        TopInfluencer topInfluencer = res.getData().getTopInfluencerList().get(0);
                        String topInfluencerId = topInfluencer.getInfluenceId();
                        callInflenceDetails(topInfluencerId);

                        /**************
                         *For Top team*
                         **************/
                        TopTeam topTeam = res.getData().getTopTeamList().get(0);
                        String topLeaderid = topTeam.getLeaderId();
                        int count = topTeam.getTotalCount();
                        callLeaderDetails(topLeaderid, count);

                    }else{
                        String resmsg = response.body().getMessage() + " ";
                        Log.v("erro400", resmsg);
                    }
                }else {
                    String resMsg = response.message() + response.code()+ " ";
                    Log.v("resMsg", resMsg);
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                String tError = t.getMessage() + " ";
                Log.v("tError", tError);
                Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void LoadPreviewImage(String imageUrl){
        String profileImage = AppConstants.PREVIEW_IMAGE + imageUrl;
        Glide.with(getActivity()).load(profileImage).into(nowShowing);
    }
    private void callLeaderDetails(String t, int count) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("leaderid", t);

        String auth = "Bearer " + token;
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        Call<LeaderResponse> responseCall = retrofitInstance.getApi().getLeaderById(auth, jsonObject);
        responseCall.enqueue(new Callback<LeaderResponse>() {
            @Override
            public void onResponse(Call<LeaderResponse> call, Response<LeaderResponse> response) {
                if (response.isSuccessful()){
                    int resCode = response.body().getStatusCode();
                    if (resCode==200){
                        LeaderResponse res = response.body();
                        String fullname = res.getLeaderData().get(0).getFullname();
                        String username = res.getLeaderData().get(0).getUsername();
                        String profileImage = AppConstants.PROFILE_URL + res.getLeaderData().get(0).getProfileImage();

                        Glide.with(getActivity()).load(profileImage).into(ttImageView);
                        ttFullname.setText(fullname);
                        ttUsername.setText(username);
                        String value = String.valueOf(count);
                        ttVote.setText(value);


                    }else if (resCode == 400){
                        String resmsg = response.body().getMessage() + " ";
                        Log.v("erro400", resmsg);
                    }
                }else {
                    String resMsg = response.message() + response.code()+ " ";
                    Log.v("resMsg", resMsg);
                }
            }

            @Override
            public void onFailure(Call<LeaderResponse> call, Throwable t) {
                String tError = t.getMessage() + " ";
                Log.v("tError", tError);
                Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void callInflenceDetails(String topInfluencerId) {

    }

    private void callLoadVideoUrl(String videoUrl) {
        String youtubID = AppConstants.extractYTId(videoUrl);
        getLifecycle().addObserver(youTubePlayerView);
        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NotNull @NonNull YouTubePlayer youTubePlayer) {
                youTubePlayer.loadVideo(youtubID, 0f);
            }
        });
    }

    private void init(View view) {
        mRvLiveNow = view.findViewById(R.id.rv_live_now);
        mRvTrending = view.findViewById(R.id.rv_trending);
        circleImageView = view.findViewById(R.id.notification_image);
    }

}
