package kol.ignite.user.ui.reset;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableField;

import com.google.gson.JsonObject;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.NewPasswordResponse;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.helper.ValidationUtils;
import kol.ignite.user.utils.rx.SchedulerProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetViewModel extends BaseViewModel<ResetNavigator> {

    public ObservableField<String> newp = new ObservableField<>("");
    public ObservableField<String> retype = new ObservableField<>("");

    ResetViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void setNavigator(ResetNavigator navigator) {
        super.setNavigator(navigator);
    }

    void reset(String TOKEN){
        String mNewPassword = newp.get();
        String mRetype = retype.get();

        if (TextUtils.isEmpty(mNewPassword)){
            getNavigator().setEditTextError(1);
            return;
        }

        if (TextUtils.isEmpty(mRetype)){
            getNavigator().setEditTextError(2);
            return;
        }

        if (mNewPassword.equals(mRetype)){

            if (ValidationUtils.isPasswordValid(mNewPassword)){
                getNavigator().showSpinLoader();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("token", TOKEN);
                jsonObject.addProperty("newpassword", mNewPassword);

                RetrofitInstance retrofitInstance = new RetrofitInstance();
                Call<NewPasswordResponse> responseCall = retrofitInstance.getApi().setNewPassword(jsonObject);
                responseCall.enqueue(new Callback<NewPasswordResponse>() {
                    @Override
                    public void onResponse(Call<NewPasswordResponse> call, Response<NewPasswordResponse> response) {
                        getNavigator().hideSpinLoader();
                        if (response.isSuccessful()){
                            int resCode = response.body().getStatuscode();
                            if (resCode ==200){
                                String resMsg = response.body().getMessage();
                                getNavigator().toastMsg(resMsg);
                            }else if(resCode == 400){
                                String resMsg = response.body().getMessage();
                                getNavigator().toastMsg(resMsg);
                            }
                        }else{
                            int resCod = response.code();
                            Log.v("reCode", resCod + " ");
                        }
                    }

                    @Override
                    public void onFailure(Call<NewPasswordResponse> call, Throwable t) {
                        getNavigator().hideSpinLoader();
                        String tError = t.getMessage() + " ";
                        Log.v("tError", tError);
                        getNavigator().toastMsg("Please check your internet connection");
                    }
                });
            }else{
                getNavigator().toastMsg("Password must be more than 6 character");
            }
        }else{
            getNavigator().toastMsg("Password does not match");
        }

    }
}
