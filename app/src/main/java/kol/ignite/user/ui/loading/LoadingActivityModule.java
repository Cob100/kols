package kol.ignite.user.ui.loading;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.R;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class LoadingActivityModule {
    @Provides
    LoadingViewModel provideLoadingViewModel(DataManager dataManager,
                                             SchedulerProvider schedulerProvider){
        return new LoadingViewModel(dataManager, schedulerProvider);
    }

    @Provides
    int provideLayoutId(){
        return R.layout.activity_loading;
    }
}

