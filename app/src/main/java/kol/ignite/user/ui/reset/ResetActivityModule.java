package kol.ignite.user.ui.reset;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.R;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class ResetActivityModule {
    @Provides
    ResetViewModel provideResetViewModel(DataManager dataManager, SchedulerProvider schedulerProvider){
        return new ResetViewModel(dataManager, schedulerProvider);
    }

    @Provides
    int provideLayoutId(){
        return R.layout.activity_reset;
    }
}
