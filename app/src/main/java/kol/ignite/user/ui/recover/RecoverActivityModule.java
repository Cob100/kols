package kol.ignite.user.ui.recover;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.R;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class RecoverActivityModule {

    @Provides
    RecoverViewModel provideRecoverViewModel(DataManager dataManager, SchedulerProvider
                                             schedulerProvider){
        return new RecoverViewModel(dataManager, schedulerProvider);
    }
    @Provides
    int provideLayoutId(){
        return R.layout.activity_recover;
    }
}
