package kol.ignite.user.ui.fragment.inbox;

import android.util.Log;

import java.util.List;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.InboxResponse;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.rx.SchedulerProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InboxViewModel extends BaseViewModel<InboxNavigator> {

    InboxViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    void callInboxe(String TOKEN){
        String auth = "Bearer " + TOKEN;
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        Call<InboxResponse> responseCall = retrofitInstance.getApi().getInbox(auth);
        responseCall.enqueue(new Callback<InboxResponse>() {
            @Override
            public void onResponse(Call<InboxResponse> call, Response<InboxResponse> response) {
                if (response.isSuccessful()){
                    int resCode = response.body().getStatuscode();
                    if (resCode ==200){
                        InboxResponse res = response.body();
                        List<InboxResponse.InboxData.Inboxes> inboxList = res.getInboxData().getInboxesList();
                        getNavigator().setInboxList(inboxList);
                    }else if(resCode == 400) {
                        String resmsg = response.body().getMessage() + " ";
                        getNavigator().toastMsg(resmsg);
                    }

                }else{
                    String resError = response.code() + " " + response.message();
                    Log.v("resError", resError);
                }
            }

            @Override
            public void onFailure(Call<InboxResponse> call, Throwable t) {
                String tError = t.getMessage() + " ";
                Log.v("tError", tError);
                getNavigator().toastMsg("Please check your internet connection");
            }
        });
    }

}
