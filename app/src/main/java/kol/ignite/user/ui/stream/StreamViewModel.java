package kol.ignite.user.ui.stream;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.rx.SchedulerProvider;

public class StreamViewModel extends BaseViewModel<StreamNavigator> {
    public StreamViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void setNavigator(StreamNavigator navigator) {
        super.setNavigator(navigator);
    }
}
