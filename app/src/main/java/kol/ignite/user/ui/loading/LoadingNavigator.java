package kol.ignite.user.ui.loading;

public interface LoadingNavigator {
    void loadSignIn();
    void loadRegister();
}
