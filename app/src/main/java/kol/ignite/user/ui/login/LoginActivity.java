package kol.ignite.user.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import kol.ignite.user.BR;
import kol.ignite.user.R;
import kol.ignite.user.data.local.prefs.AppPreferencesHelper;
import kol.ignite.user.data.model.User;
import kol.ignite.user.databinding.ActivityLoginBinding;
import kol.ignite.user.ui.base.BaseActivity;
import kol.ignite.user.ui.home.MainActivity;
import kol.ignite.user.ui.recover.RecoverActivity;
import kol.ignite.user.utils.manager.AlertManager;
import kol.ignite.user.utils.manager.PrefManager;

public class LoginActivity extends BaseActivity<ActivityLoginBinding, LoginViewModel>
    implements LoginNavigator{

    @Inject
    LoginViewModel loginViewModel;

    @Inject
    int layoutId = R.layout.activity_login;

    ActivityLoginBinding mBinder;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doBinding();
    }

    private void doBinding() {
        mBinder = getViewDataBinding();
        mBinder.setLoginviewmodel(loginViewModel);
        mBinder.setNavigator(this);
        getViewModel().setNavigator(this);
    }

    @Override
    public LoginViewModel getViewModel() {
        return loginViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.loginviewmodel;
    }

    @Override
    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public void loadRecover() {
        startActivity(new Intent(LoginActivity.this, RecoverActivity.class));
    }



    @Override
    public void loadLogin() {
        getViewModel().login();
    }

    @Override
    public void setError(int i) {
        switch (i) {
            case 1:
                mBinder.email.setError("Please, Enter this field");
                mBinder.email.requestFocus();
                break;
            case 2:
                mBinder.password.setError("Please, enter this field");
                mBinder.password.requestFocus();
                break;
        }
    }

    @Override
    public void toastError(String value) {
        AlertManager alertManager = new AlertManager(this);
        alertManager.showLongToast(value);
    }


    @Override
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    AlertManager alertManagerSpin;
    @Override
    public void showSpinLoader() {
        alertManagerSpin = new AlertManager(this);
        alertManagerSpin.showCustomDialog();
    }

    @Override
    public void hideSpinLoader() {
        alertManagerSpin.cancelCustomDialog();
    }

    @Override
    public void loadHome(String token, String fullname, String username, String email, String id) {
        PrefManager.getInstance(LoginActivity.this).setToken(token);
        User user = new User(id, fullname, username,email, email);
        AppPreferencesHelper.getInstance(LoginActivity.this).saveUser(user);
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
    }
}
