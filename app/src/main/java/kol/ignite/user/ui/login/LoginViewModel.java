package kol.ignite.user.ui.login;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableField;

import com.google.gson.JsonObject;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.LoginResponse;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.helper.ValidationUtils;
import kol.ignite.user.utils.rx.SchedulerProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends BaseViewModel<LoginNavigator> {

    public ObservableField<String> email = new ObservableField<>("");
    public ObservableField<String> password = new ObservableField<>("");

    LoginViewModel(DataManager dataManager,
                          SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void setNavigator(LoginNavigator navigator) {
        super.setNavigator(navigator);
    }

    public void login(){
        String mEmail = email.get();
        String mPassword = password.get();

        if (TextUtils.isEmpty(mEmail)){
            getNavigator().setError(1);
            return;
        }

        if (TextUtils.isEmpty(mPassword)){
            getNavigator().setError(2);
            return;
        }

        if (ValidationUtils.isEmailValid(mEmail)){
            if (ValidationUtils.isPasswordValid(mPassword)){
                getNavigator().showSpinLoader();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("email", mEmail);
                jsonObject.addProperty("password", mPassword);

                RetrofitInstance retrofitInstance = new RetrofitInstance();
                Call<LoginResponse> responseCall = retrofitInstance.getApi().login(jsonObject);

                responseCall.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        getNavigator().hideSpinLoader();
                        if (response.isSuccessful()){
                            int resCode = response.body().getStatuscode();
                            if (resCode ==200){
                                LoginResponse res = response.body();
                                String token = res.getToken();
                                String id = res.getData().getUser().getId();
                                String fullname = res.getData().getUser().getFullname();
                                String username = "@" + res.getData().getUser().getUsername();
                                String email = res.getData().getUser().getEmail();
                                getNavigator().toastError(response.body().getMessage());
                                getNavigator().loadHome(token, fullname, username, email, id);
                            }else if(resCode == 400) {
                                getNavigator().toastError(response.body().getMessage());
                            }
                        }else{
                            String resError = response.code() + " " + response.message();
                            Log.v("resError", resError);
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        getNavigator().hideSpinLoader();
                        String tError = t.getMessage() + " ";
                        Log.v("tError", tError);
                        getNavigator().toastError("Please check your internet connection");
                    }
                });

            }else{
                getNavigator().toastError("Password must be more than 6 character");
            }
        }else{
            getNavigator().toastError("Invalid Email");
        }

    }
}
