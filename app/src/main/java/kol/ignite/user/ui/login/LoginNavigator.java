package kol.ignite.user.ui.login;

public interface LoginNavigator {
    void loadRecover();
    void onBack();
    void loadLogin();

    void setError(int i);

    void toastError(String s);

    void showSpinLoader();

    void hideSpinLoader();

    void loadHome(String token, String fullname, String username, String email, String id);
}
