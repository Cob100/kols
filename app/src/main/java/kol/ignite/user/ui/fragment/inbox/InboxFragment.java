package kol.ignite.user.ui.fragment.inbox;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.todkars.shimmer.ShimmerRecyclerView;

import java.util.List;

import kol.ignite.user.R;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.InboxResponse;
import kol.ignite.user.ui.adapter.InboxAdapter;
import kol.ignite.user.ui.detailinbox.InboxDetailActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class InboxFragment extends Fragment{

    LinearLayoutManager manager;
    InboxAdapter.ItemClickListener itemClickListener;
    ShimmerRecyclerView rvInbox;

    String Token;


    public InboxFragment(String _token) {
        this.Token = _token + " ";
        Log.v("toooken", Token);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inbox, container, false);
        initAll(view);
        return view;
    }


    private void initAll(View view) {
        manager = new LinearLayoutManager(getActivity());
        itemClickListener = (title, details) -> {
            Intent intent = new Intent(getActivity(), InboxDetailActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("details", details);
            startActivity(intent);
        };
        rvInbox = view.findViewById(R.id.rv_inbox);
        rvInbox.showShimmer();
        callInboxe(Token);
    }

    void callInboxe(String TOKEN){
        String auth = "Bearer " + TOKEN;
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        Call<InboxResponse> responseCall = retrofitInstance.getApi().getInbox(auth);
        responseCall.enqueue(new Callback<InboxResponse>() {
            @Override
            public void onResponse(Call<InboxResponse> call, Response<InboxResponse> response) {
                if (response.isSuccessful()){
                    rvInbox.hideShimmer();
                    int resCode = response.body().getStatuscode();
                    if (resCode ==200){
                        InboxResponse res = response.body();
                        List<InboxResponse.InboxData.Inboxes> inboxList = res.getInboxData().getInboxesList();
                        RecyclerView.Adapter adapter = new InboxAdapter(itemClickListener, inboxList);
                        rvInbox.setLayoutManager(manager,R.layout.inbox);
                        rvInbox.setAdapter(adapter);
                    }else if(resCode == 400) {
                        String resmsg = response.body().getMessage() + " ";
                    }

                }else{
                    String resError = response.code() + " " + response.message();
                    Log.v("resError", resError);
                }
            }

            @Override
            public void onFailure(Call<InboxResponse> call, Throwable t) {
                rvInbox.hideShimmer();
                String tError = t.getMessage() + " ";
                Log.v("tError", tError);
                Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
