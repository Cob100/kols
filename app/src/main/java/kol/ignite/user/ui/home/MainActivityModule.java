package kol.ignite.user.ui.home;

import androidx.lifecycle.ViewModelProvider;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import kol.ignite.user.R;
import kol.ignite.user.ViewModelProviderFactory;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.ui.fragment.inbox.InboxFragment;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class MainActivityModule {
    @Provides
    MainViewModel providesMainViewModel(DataManager dataManager,
                                        SchedulerProvider schedulerProvider) {
        return new MainViewModel(dataManager, schedulerProvider);
    }

   /* @Provides
    ViewModelProvider.Factory mainViewModelProvider(MainViewModel mainViewModel) {
        return new ViewModelProviderFactory<>(mainViewModel);
    }*/

    @Provides
    int provideLayoutId() {
        return R.layout.activity_main;
    }
}
