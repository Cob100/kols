package kol.ignite.user.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kol.ignite.user.R;
import kol.ignite.user.network.account.response.model.LiveNow;

public class LiveNowAdapter extends RecyclerView.Adapter<LiveNowAdapter.ViewHolder> {

        ItemClickListener itemClickListener;
        List<LiveNow> liveNows;

        public LiveNowAdapter(ItemClickListener _item, List<LiveNow> liveNowList){
                this.itemClickListener = _item;
                this.liveNows = liveNowList;
        }


        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.live_now, viewGroup, false);
                ViewHolder viewHolder = new ViewHolder(view);
                return viewHolder;
                }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
            ViewHolder viewHolder = holder;

            //viewHolder.mAddressBox.setText(names[i]);

            viewHolder.mAlert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClicked();
                }
            });
        }

        @Override
        public int getItemCount() {
                if(null == liveNows) return 0;
                return this.liveNows.size();
                }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mAddressBox;
            public LinearLayout mAlert;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                mAddressBox = itemView.findViewById(R.id.title);
                mAlert = itemView.findViewById(R.id.fl_live_now);
            }
        }

        public interface ItemClickListener{
            void onItemClicked();
        }
}

