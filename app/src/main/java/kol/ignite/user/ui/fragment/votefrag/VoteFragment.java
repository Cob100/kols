package kol.ignite.user.ui.fragment.votefrag;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.todkars.shimmer.ShimmerRecyclerView;

import java.util.List;

import kol.ignite.user.R;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.DefaultResponse;
import kol.ignite.user.network.account.response.LeadershipResponse;
import kol.ignite.user.ui.adapter.VoteAdapter;
import kol.ignite.user.ui.profile.ProfileActivity;
import kol.ignite.user.utils.manager.AlertManager;
import kol.ignite.user.utils.manager.PrefManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class VoteFragment extends Fragment {

    ShimmerRecyclerView mRv;
    LinearLayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    VoteAdapter.ItemClickListener itemClickListener;
    public VoteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vote, container, false);

        mRv = view.findViewById(R.id.rv_vote);
        layoutManager = new LinearLayoutManager(getActivity());

        /*VoteAdapter.ItemClickListener itemClickListener = (fullname, username, url) -> {
            
        };*/

        itemClickListener = new VoteAdapter.ItemClickListener() {
            @Override
            public void onItemClicked(String fullname, String username, String url, String id) {
                Intent intent = new Intent(VoteFragment.this.getActivity(), ProfileActivity.class);
                intent.putExtra("fullname", fullname);
                intent.putExtra("username", username);
                intent.putExtra("profileUrl", url);
                intent.putExtra("id", id);
                VoteFragment.this.startActivity(intent);
            }

            @Override
            public void vote(String id) {
                //callInfluencerVote(id);
            }
        };

        mRv.showShimmer();
        getLeadershipBoard();
        return view;
    }


    private void getLeadershipBoard(){
        String Token = PrefManager.getInstance(getActivity()).getToken();
        String auth  = "Bearer " + Token;
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        Call<LeadershipResponse> responseCall = retrofitInstance.getApi().leaderBoard(auth);
        responseCall.enqueue(new Callback<LeadershipResponse>() {
            @Override
            public void onResponse(Call<LeadershipResponse> call, Response<LeadershipResponse> response) {
                if(response.isSuccessful()){
                    int resCode = response.body().getStatuscode();
                    if (resCode == 200){
                        mRv.hideShimmer();
                        List<LeadershipResponse.LData.LeadershipBoard> leadershipBoardList = response.body()
                                .getData().leadershipBoardList;

                        mRv.setLayoutManager(layoutManager, R.layout.vote);
                        adapter = new VoteAdapter(itemClickListener, leadershipBoardList, getActivity());
                        mRv.setAdapter(adapter);

                    }else if(resCode == 400){
                        String resMsg = response.body().getMessage() + " ";
                        Toast.makeText(getActivity(), resMsg, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LeadershipResponse> call, Throwable t) {
                String tError = t.getMessage() + " ";
                Log.v("tError", tError);
                Toast.makeText(getActivity(), "Please check your internet connection",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


}
