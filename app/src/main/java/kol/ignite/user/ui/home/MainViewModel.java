package kol.ignite.user.ui.home;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.rx.SchedulerProvider;

public class MainViewModel extends BaseViewModel<MainNavigator> {

    MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void setNavigator(MainNavigator navigator) {
        super.setNavigator(navigator);
    }

    public void onTabSelected(int tab) {
        getNavigator().resetTabs();
        switch (tab) {
            case 1:
                showHomeFragment();
                break;
            case 2:
                showExploreFragment();
                break;
            case 3:
                showShareFragment();
                break;
            case 4:
                showNewsFragment();
                break;
            case 5:
                showProfileFragment();
                break;
        }
    }



    private void showHomeFragment() {
        getNavigator().replaceFragment(1);
    }

    private void showExploreFragment() {
        getNavigator().replaceFragment(2);
    }

    private void showShareFragment() {
        getNavigator().replaceFragment(3);
    }

    private void showNewsFragment() {
        getNavigator().replaceFragment(4);
    }

    private void showProfileFragment() {
        getNavigator().replaceFragment(5);
    }



}
