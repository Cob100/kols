package kol.ignite.user.ui.profile;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.R;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class ProfileActivityModule {
    @Provides
    ProfileViewModel provideProfileViewModel(DataManager dataManager,
                                             SchedulerProvider schedulerProvider){
        return new ProfileViewModel(dataManager, schedulerProvider);
    }

    @Provides
    int provideLayoutId(){
        return R.layout.activity_profile;
    }
}
