package kol.ignite.user.ui.recover;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableField;

import com.google.gson.JsonObject;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.RecoverResponse;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.helper.ValidationUtils;
import kol.ignite.user.utils.rx.SchedulerProvider;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecoverViewModel extends BaseViewModel<RecoverNavigator> {

    public ObservableField<String> email = new ObservableField<>("");

    public RecoverViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void setNavigator(RecoverNavigator navigator) {
        super.setNavigator(navigator);
    }

    void recover(){
        String mEmail = email.get();

        if (TextUtils.isEmpty(mEmail)){
            getNavigator().setEditTextError(1);
            return;
        }

        if (ValidationUtils.isEmailValid(mEmail)){
            getNavigator().showSpinLoader();


            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email", mEmail);

            RetrofitInstance retrofitInstance = new RetrofitInstance();
            Call<RecoverResponse> recoverResponse = retrofitInstance.getApi().recov(jsonObject);

            recoverResponse.enqueue(new Callback<RecoverResponse>() {
                @Override
                public void onResponse(Call<RecoverResponse> call, Response<RecoverResponse> response) {
                    getNavigator().hideSpinLoader();
                    if (response.isSuccessful()){
                        int resCode = response.body().getStatuscode();
                        if (resCode == 200){
                            /******
                             * I think we should load new set pin here*/
                            getNavigator().toastMsg("Recover successfully");
                            getNavigator().loadHome();
                        }else if (resCode ==400){
                            String resError = response.code() + " " + response.body().getMessage();
                            Log.v("resError", resError);
                        }
                    }else{
                        String resError = response.code() + " " + response.message();
                        Log.v("resError", resError);
                    }
                }

                @Override
                public void onFailure(Call<RecoverResponse> call, Throwable t) {
                    getNavigator().hideSpinLoader();
                    String tError = t.getMessage() + " ";
                    Log.v("tError", tError);
                    getNavigator().toastMsg("Please check your internet connection");
                }
            });
        }else{
            getNavigator().toastMsg("Invalid Email");
        }
    }

}
