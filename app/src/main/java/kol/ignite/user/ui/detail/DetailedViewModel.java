package kol.ignite.user.ui.detail;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableField;

import com.google.gson.JsonObject;

import kol.ignite.user.data.DataManager;
import kol.ignite.user.network.account.RetrofitInstance;
import kol.ignite.user.network.account.response.DefaultResponse;
import kol.ignite.user.ui.base.BaseViewModel;
import kol.ignite.user.utils.rx.SchedulerProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailedViewModel extends BaseViewModel<DetailedNavigator> {

    public ObservableField<String> comment = new ObservableField<>("");

    public DetailedViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void setNavigator(DetailedNavigator navigator) {
        super.setNavigator(navigator);
    }

    void comment(String TOKEN, String username, String id){

        String mComment = comment.get();

        if (TextUtils.isEmpty(mComment)){
            getNavigator().toastMsg("Please, add comment field");
            return;
        }

        getNavigator().showSpinLoader();
        String auth = "Bearer " + TOKEN;

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", username);
        jsonObject.addProperty("comment", mComment);
        jsonObject.addProperty("episode", id);


        RetrofitInstance retrofitInstance = new RetrofitInstance();
        Call<DefaultResponse> responseCall = retrofitInstance.getApi().sendComment(auth, jsonObject);
        responseCall.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                getNavigator().hideSpinLoader();
                if (response.isSuccessful()){
                    int resCode = response.body().getStatuscode();
                    if (resCode ==200){
                        DefaultResponse res = response.body();
                        String token = res.getMessage()+ " ";
                        getNavigator().toastMsg(token);
                    }else if(resCode == 400) {
                        getNavigator().toastMsg(response.body().getMessage());
                    }
                }else{
                    String resError = response.code() + " " + response.message();
                    Log.v("resError", resError);
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                getNavigator().hideSpinLoader();
                String tError = t.getMessage() + " ";
                Log.v("tError", tError);
                getNavigator().toastMsg("Please check your internet connection");
            }
        });

    }
}
