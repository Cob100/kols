package kol.ignite.user.di.builder;



import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import kol.ignite.user.ui.detail.DetailedActivity;
import kol.ignite.user.ui.detail.DetailedActivityModule;
import kol.ignite.user.ui.detailinbox.InboxDetailActivity;
import kol.ignite.user.ui.detailinbox.InboxDetailActivityModule;
import kol.ignite.user.ui.detailinbox.InboxDetailViewModel;
import kol.ignite.user.ui.fragment.inbox.InboxFragmentModule;
import kol.ignite.user.ui.fragment.inbox.InboxFragmentProvider;
import kol.ignite.user.ui.home.MainActivity;
import kol.ignite.user.ui.home.MainActivityModule;
import kol.ignite.user.ui.loading.LoadingActivity;
import kol.ignite.user.ui.loading.LoadingActivityModule;
import kol.ignite.user.ui.login.LoginActivity;
import kol.ignite.user.ui.login.LoginActivityModule;
import kol.ignite.user.ui.profile.ProfileActivity;
import kol.ignite.user.ui.profile.ProfileActivityModule;
import kol.ignite.user.ui.recover.RecoverActivity;
import kol.ignite.user.ui.recover.RecoverActivityModule;
import kol.ignite.user.ui.reset.ResetActivity;
import kol.ignite.user.ui.reset.ResetActivityModule;
import kol.ignite.user.ui.signup.SignUpActivity;
import kol.ignite.user.ui.signup.SignUpActivityModule;
import kol.ignite.user.ui.stream.StreamActivity;
import kol.ignite.user.ui.stream.StreamActivityModule;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity mainActivity();

    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    abstract LoginActivity loginActivity();

    @ContributesAndroidInjector(modules = SignUpActivityModule.class)
    abstract SignUpActivity signUpActivity();

    @ContributesAndroidInjector(modules = RecoverActivityModule.class)
    abstract RecoverActivity recoverActivity();

    @ContributesAndroidInjector(modules = LoadingActivityModule.class)
    abstract LoadingActivity loadingActivity();

    @ContributesAndroidInjector(modules = StreamActivityModule.class)
    abstract StreamActivity streamActivity();

    @ContributesAndroidInjector(modules = DetailedActivityModule.class)
    abstract DetailedActivity detailedActivity();

    @ContributesAndroidInjector(modules = ProfileActivityModule.class)
    abstract ProfileActivity profileActivity();

    @ContributesAndroidInjector(modules = InboxDetailActivityModule.class)
    abstract InboxDetailActivity inboxDetailActivity();

    @ContributesAndroidInjector(modules = ResetActivityModule.class)
    abstract ResetActivity resetActivity();
}


