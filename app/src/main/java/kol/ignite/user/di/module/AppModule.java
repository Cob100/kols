package kol.ignite.user.di.module;



import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kol.ignite.user.AppConstants;
import kol.ignite.user.config.BuildConfig;
import kol.ignite.user.data.AppDataManager;
import kol.ignite.user.data.DataManager;
import kol.ignite.user.data.local.prefs.AppPreferencesHelper;
import kol.ignite.user.data.local.prefs.PreferencesHelper;
import kol.ignite.user.data.remote.ApiHelper;
import kol.ignite.user.data.remote.AppApiHelper;
import kol.ignite.user.di.ApiInfo;
import kol.ignite.user.di.PreferenceInfo;
import kol.ignite.user.utils.rx.AppSchedulerProvider;
import kol.ignite.user.utils.rx.SchedulerProvider;

@Module
public class AppModule {
    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @ApiInfo
    String provideApiKey() {
        return BuildConfig.API_KEY;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }


    //release on api header addition
/*
    @Provides
    @Singleton
    ApiHeader.ProtectedApiHeader provideProtectedApiHeader(@ApiInfo String apiKey,
                                                           PreferencesHelper preferencesHelper) {
        return new ApiHeader.ProtectedApiHeader(preferencesHelper.getAccessToken());
    }

    */


}
