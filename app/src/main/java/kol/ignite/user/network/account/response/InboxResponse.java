package kol.ignite.user.network.account.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InboxResponse {
    @SerializedName("statuscode")
    public int statuscode;

    public int getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(int statuscode) {
        this.statuscode = statuscode;
    }

    @SerializedName("message")
    public String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("data")
    public InboxData inboxData;

    public InboxData getInboxData() {
        return inboxData;
    }

    public void setInboxData(InboxData inboxData) {
        this.inboxData = inboxData;
    }

    public class InboxData {
        @SerializedName("inboxes")
        public List<Inboxes> inboxesList;

        public List<Inboxes> getInboxesList() {
            return inboxesList;
        }

        public void setInboxesList(List<Inboxes> inboxesList) {
            this.inboxesList = inboxesList;
        }

        public class Inboxes {
            @SerializedName("date")
            public String date;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            @SerializedName("title")
            public String title;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
            @SerializedName("message")
            public String message;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }
    }
}
