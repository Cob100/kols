package kol.ignite.user.network.account.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoResponse {
    @SerializedName("statuscode")
    public int statuscode;

    public int getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(int statuscode) {
        this.statuscode = statuscode;
    }

    @SerializedName("message")
    public String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("data")
    public videoData data;

    public videoData getData() {
        return data;
    }

    public void setData(videoData data) {
        this.data = data;
    }

    public class videoData {
        @SerializedName("episodes")
        public List<Videos> videos;

        public List<Videos> getVideos() {
            return videos;
        }

        public void setVideos(List<Videos> videos) {
            this.videos = videos;
        }

        public class Videos {
            @SerializedName("_id")
            public String id;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
            @SerializedName("title")
            public String title;

            @SerializedName("hightlightimage")
            public String hightlightimage;



            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            @SerializedName("description")
            public String desp;

            public String getDesp() {
                return desp;
            }

            public void setDesp(String desp) {
                this.desp = desp;
            }
            @SerializedName("introvideo")
            public String videoUrl;

            public String getVideoUrl() {
                return videoUrl;
            }

            public void setVideoUrl(String videoUrl) {
                this.videoUrl = videoUrl;
            }

            @SerializedName("comments")
            public List<Comments> comments;

            public List<Comments> getComments() {
                return comments;
            }

            public void setComments(List<Comments> comments) {
                this.comments = comments;
            }

            public class Comments {
                @SerializedName("_id")
                public String commentId;

                public String getCommentId() {
                    return commentId;
                }

                public void setCommentId(String commentId) {
                    this.commentId = commentId;
                }
                @SerializedName("comment")
                public String comment;

                public String getComment() {
                    return comment;
                }

                public void setComment(String comment) {
                    this.comment = comment;
                }

                @SerializedName("username")
                public String usernameComment;

                public String getUsernameComment() {
                    return usernameComment;
                }

                public void setUsernameComment(String usernameComment) {
                    this.usernameComment = usernameComment;
                }
            }
        }
    }
}
