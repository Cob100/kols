package kol.ignite.user.network.account.response.model;

import com.google.gson.annotations.SerializedName;

public class TopInfluencer {
    @SerializedName("_id")
    public String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @SerializedName("influencerid")
    public String influenceId;

    public String getInfluenceId() {
        return influenceId;
    }

    public void setInfluenceId(String influenceId) {
        this.influenceId = influenceId;
    }

    @SerializedName("totalcount")
    public int totalCount;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
