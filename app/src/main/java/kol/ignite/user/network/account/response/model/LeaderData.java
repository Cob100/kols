package kol.ignite.user.network.account.response.model;

import com.google.gson.annotations.SerializedName;

public class LeaderData {
    @SerializedName("fullname")
    public String fullname;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @SerializedName("artistname")
    public String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @SerializedName("profileimage")
    public String profileImage;

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
