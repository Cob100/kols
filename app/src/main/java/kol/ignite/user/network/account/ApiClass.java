package kol.ignite.user.network.account;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClass {
    public static final String BASE_URL = "http://18.216.101.201:4000/";
    private static Retrofit retrofit = null;

    public static Retrofit getApi() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
