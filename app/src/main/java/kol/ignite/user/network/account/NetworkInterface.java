package kol.ignite.user.network.account;

import com.google.gson.JsonObject;

import kol.ignite.user.network.account.response.DefaultResponse;
import kol.ignite.user.network.account.response.HomeResponse;
import kol.ignite.user.network.account.response.InboxResponse;
import kol.ignite.user.network.account.response.LeaderResponse;
import kol.ignite.user.network.account.response.LeadershipResponse;
import kol.ignite.user.network.account.response.LoginResponse;
import kol.ignite.user.network.account.response.NewPasswordResponse;
import kol.ignite.user.network.account.response.RecoverResponse;
import kol.ignite.user.network.account.response.SignupResponse;
import kol.ignite.user.network.account.response.VideoResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface NetworkInterface {

    @POST("users/login")
    Call<LoginResponse> login(
            @Body JsonObject object);

    @POST("users/register")
    Call<SignupResponse> reg(
            @Body JsonObject object
    );

    @POST("users/resetrequest")
    Call<RecoverResponse> recov(
            @Body JsonObject object
    );

    @POST("users/setnewpassword")
    Call<NewPasswordResponse> setNewPassword(
            @Body JsonObject object
    );

    @POST("users/leadershipboard")
    Call<LeadershipResponse> leaderBoard(
            @Header("Authorization") String auth
    );

    @POST("users/vote")
    Call<DefaultResponse> voteInfluencer(
            @Header("Authorization") String auth,
            @Body JsonObject object
    );

    @POST("users/inboxes")
    Call<InboxResponse> getInbox(
            @Header("Authorization") String auth);

    @POST("users/episodes")
    Call<VideoResponse> getVideos();

    @POST("users/comment")
    Call<DefaultResponse> sendComment(
            @Header("Authorization") String auth,
            @Body JsonObject jsonObject
    );

    @POST("users/home")
    Call<HomeResponse> getHome(
            @Header("Authorization") String auth
    );

    @POST("users/leader")
    Call<LeaderResponse> getLeaderById(
            @Header("Authorization") String auth,
            @Body JsonObject jsonObject
    );
}
