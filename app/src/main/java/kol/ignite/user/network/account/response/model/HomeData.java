package kol.ignite.user.network.account.response.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import kol.ignite.user.network.account.response.VideoResponse;

public class HomeData {
    @SerializedName("liveNow")
    public List<LiveNow> liveNowList;

    public List<LiveNow> getLiveNowList() {
        return liveNowList;
    }

    public void setLiveNowList(List<LiveNow> liveNowList) {
        this.liveNowList = liveNowList;
    }

    @SerializedName("showingNow")
    public List<VideoResponse.videoData.Videos>  showingNowList;

    public List<VideoResponse.videoData.Videos> getShowingNowList() {
        return showingNowList;
    }

    public void setShowingNowList(List<VideoResponse.videoData.Videos> showingNowList) {
        this.showingNowList = showingNowList;
    }

    @SerializedName("topTeam")
    public List<TopTeam> topTeamList;

    public List<TopTeam> getTopTeamList() {
        return topTeamList;
    }

    public void setTopTeamList(List<TopTeam> topTeamList) {
        this.topTeamList = topTeamList;
    }

    @SerializedName("topInfluencer")
    public List<TopInfluencer> topInfluencerList;

    public List<TopInfluencer> getTopInfluencerList() {
        return topInfluencerList;
    }

    public void setTopInfluencerList(List<TopInfluencer> topInfluencerList) {
        this.topInfluencerList = topInfluencerList;
    }
}
