package kol.ignite.user.network.account.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import kol.ignite.user.network.account.response.model.LeaderData;

public class LeaderResponse {
    @SerializedName("statuscode")
    public int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @SerializedName("message")
    public String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("data")
    public List<LeaderData> leaderData;

    public List<LeaderData> getLeaderData() {
        return leaderData;
    }

    public void setLeaderData(List<LeaderData> leaderData) {
        this.leaderData = leaderData;
    }
}
