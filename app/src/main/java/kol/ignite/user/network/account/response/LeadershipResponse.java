package kol.ignite.user.network.account.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadershipResponse {
    @SerializedName("statuscode")
    public int statuscode;

    public int getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(int statuscode) {
        this.statuscode = statuscode;
    }

    @SerializedName("data")
    public LData data;

    public LData getData() {
        return data;
    }

    public void setData(LData data) {
        this.data = data;
    }

    @SerializedName("message")
    public String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class LData {
        @SerializedName("leadershipboard")
        public List<LeadershipBoard> leadershipBoardList;

        public List<LeadershipBoard> getLeadershipBoardList() {
            return leadershipBoardList;
        }

        public void setLeadershipBoardList(List<LeadershipBoard> leadershipBoardList) {
            this.leadershipBoardList = leadershipBoardList;
        }

        public class LeadershipBoard {
            @SerializedName("influencer")
            public List<Influencer> influencers;

            public List<Influencer> getInfluencers() {
                return influencers;
            }

            public void setInfluencers(List<Influencer> influencers) {
                this.influencers = influencers;
            }

            @SerializedName("position")
            public int position;

            public int getPosition() {
                return position;
            }

            public void setPosition(int position) {
                this.position = position;
            }

            @SerializedName("totalPoint")
            public int totalPoint;

            public int getTotalPoint() {
                return totalPoint;
            }

            public void setTotalPoint(int totalPoint) {
                this.totalPoint = totalPoint;
            }

            public class Influencer {

                @SerializedName("_id")
                public String influencerId;

                public String getInfluencerId() {
                    return influencerId;
                }

                public void setInfluencerId(String influencerId) {
                    this.influencerId = influencerId;
                }

                @SerializedName("fullname")
                public String fullName;

                public String getFullName() {
                    return fullName;
                }

                public void setFullName(String fullName) {
                    this.fullName = fullName;
                }

                @SerializedName("artistname")
                public String username;

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                @SerializedName("profileimage")
                public String profileImage;

                public String getProfileImage() {
                    return profileImage;
                }

                public void setProfileImage(String profileImage) {
                    this.profileImage = profileImage;
                }
            }
        }
    }
}
