package kol.ignite.user.network.account.response.model;

import com.google.gson.annotations.SerializedName;

public class ShowingNow {
    @SerializedName("introvideo")
    public String videoUrl;

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
