package kol.ignite.user.network.account.response;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("statuscode")
    public int statuscode;

    public int getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(int statuscode) {
        this.statuscode = statuscode;
    }

    @SerializedName("token")
    public String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @SerializedName("message")
    public String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("data")
    public loginData data;

    public loginData getData() {
        return data;
    }

    public void setData(loginData data) {
        this.data = data;
    }


    public class loginData {

        @SerializedName("user")
        public userData user;

        public userData getUser() {
            return user;
        }

        public void setUser(userData user) {
            this.user = user;
        }

        public class userData {
            @SerializedName("_id")
            public String id;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            @SerializedName("fullname")
            public String fullname;

            public String getFullname() {
                return fullname;
            }

            public void setFullname(String fullname) {
                this.fullname = fullname;
            }

            @SerializedName("username")
            public String username;

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
            @SerializedName("email")
            public String email;

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }
        }
    }
}

