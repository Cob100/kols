package kol.ignite.user.network.account.response;

import com.google.gson.annotations.SerializedName;

import kol.ignite.user.network.account.response.model.HomeData;

public class HomeResponse {
    @SerializedName("statuscode")
    public int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @SerializedName("message")
    public String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("data")
    public HomeData data;

    public HomeData getData() {
        return data;
    }

    public void setData(HomeData data) {
        this.data = data;
    }
}
