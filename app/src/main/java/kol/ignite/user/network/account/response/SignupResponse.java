package kol.ignite.user.network.account.response;

import com.google.gson.annotations.SerializedName;

public class SignupResponse {
    @SerializedName("statuscode")
    public int statuscode;

    public int getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(int statuscode) {
        this.statuscode = statuscode;
    }

    @SerializedName("message")
    public String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
