package kol.ignite.user.utils.helper;

import android.view.View;

public abstract class OneClickListener implements View.OnClickListener {

    private boolean clickable = true;

    @Override
    public void onClick(View v) {
        if (clickable) {
            clickable = false;
            onOneClick(v);
        }
    }

    protected abstract void onOneClick(View v);

    public void reset() {
        clickable = true;
    }
}
