package kol.ignite.user.utils.helper;

public interface CompletedJobListener {
    void onJobCompleted(Object data);
}
