package kol.ignite.user;

import android.app.Activity;
import android.app.Application;


import androidx.fragment.app.Fragment;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.support.HasSupportFragmentInjector;
import kol.ignite.user.di.component.DaggerAppComponent;

public class Kols extends Application implements HasActivityInjector {
  @Inject
  DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;


  @Override
  public void onCreate() {
    super.onCreate();
    DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this);

  }

  @Override
  public DispatchingAndroidInjector<Activity> activityInjector() {
    return activityDispatchingAndroidInjector;
  }

}
